package jab.bcbans.json;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import jab.bcbans.bukkit.commands.response.ResponseInterpreter;
import jab.bcbans.json.object.Ban;
import jab.bcbans.main.BCBans;
import jab.bcbans.main.LogSystem;

/**
 * 
 * @author JabJabJab
 *
 */
public class JSONBanGlobal extends JSONInterpreter
{
	String reason;

	public JSONBanGlobal(String playerName, String moderatorName, String reason) 
	{
		super(playerName,moderatorName);
		this.reason = reason;
	}

	private void dealWithBan() 
	{
		if(playerName.equals(moderatorName))
		{
			ResponseInterpreter.sendPlayerMessage(moderatorName, "You cannot ban yourself!");
			return;
		}
		Player player;
		Ban ban = JSONHandler.GlobalBanPlayer(playerName, moderatorName, reason);
		if(!ban.created_at.isEmpty())
		{
			player = Bukkit.getPlayer(playerName);
			if(player == null)
			{
				OfflinePlayer offlinePlayer = Bukkit.getOfflinePlayer(playerName);
				offlinePlayer.setBanned(true);
			}
			else
			{				
				player.setBanned(true);
				player.kickPlayer(BCBans.configuration.getDefaultBanMessage());
			}
		}
		if(!moderatorName.equals("(Console)"))
		{
			ResponseInterpreter.sendPlayerMessage(moderatorName, ChatColor.GREEN + "\u2554" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + ChatColor.BOLD + "\u25ba");
			ResponseInterpreter.sendPlayerMessage(moderatorName, ChatColor.GREEN + "\u2551" + " " + ChatColor.BOLD + "\u2553" + ChatColor.BOLD + "\u25ba" + ChatColor.RESET  + ChatColor.GREEN + ChatColor.BOLD + "" + ChatColor.ITALIC + "Globally Banning Player " + ChatColor.AQUA + "" + ChatColor.BOLD + "" + ChatColor.ITALIC + playerName);
			ResponseInterpreter.sendPlayerMessage(moderatorName, ChatColor.GREEN + "\u2551" + " " + ChatColor.BOLD + "\u2551");
			if(!ban.created_at.isEmpty())
			{
				ResponseInterpreter.sendPlayerMessage(moderatorName, ChatColor.GREEN + "\u2551" + " " + ChatColor.BOLD + "\u2559" + "\u2500" + "\u2500" + "\u2500" + ChatColor.RESET + "" + ChatColor.GREEN + "\u25ba" + ChatColor.RESET + "" + ChatColor.GREEN + "" + ChatColor.BOLD + "" + ChatColor.ITALIC + 				
						"Message: " +  ChatColor.RESET + ChatColor.GREEN + "" + ChatColor.ITALIC + "Successful ban.");
			}
			else
			{
				ResponseInterpreter.sendPlayerMessage(moderatorName, ChatColor.GREEN + "\u2551" + " " + ChatColor.BOLD + "\u2559" + "\u2500" + "\u2500" + "\u2500" + ChatColor.RESET + "" + ChatColor.GREEN + "\u25ba" + ChatColor.RESET + "" + ChatColor.RED + "" + ChatColor.BOLD + "" + ChatColor.ITALIC + 
						"Error: " +  ChatColor.RESET + ChatColor.GREEN + "" + ChatColor.ITALIC + ban.error);

			}
			ResponseInterpreter.sendPlayerMessage(moderatorName, ChatColor.GREEN + "\u2551");
			
			ResponseInterpreter.sendPlayerMessage(moderatorName, ChatColor.GREEN + "\u255a" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + ChatColor.BOLD + "\u25ba");			
		}
		else
		{
			if(!ban.created_at.isEmpty())
			{				
				LogSystem.logEvent(LogSystem.LOG_INFO, "Player Globally Banned: " + playerName);
			}
			else
			{
				LogSystem.logEvent(LogSystem.LOG_ERROR, ban.error);
			}
		}
		
	}


	@Override
	public void run() 
	{
		dealWithBan();
	}

}
