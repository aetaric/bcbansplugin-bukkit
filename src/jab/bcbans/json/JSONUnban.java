package jab.bcbans.json;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;

import jab.bcbans.bukkit.commands.response.ResponseInterpreter;
import jab.bcbans.json.object.UnBan;
import jab.bcbans.main.LogSystem;

public class JSONUnban extends JSONInterpreter
{
	
	public JSONUnban(String playerName, String moderatorName) 
	{
		super(playerName,moderatorName);
		
	}

	private void dealWithUnban() 
	{
		UnBan unban = JSONHandler.UnBanPlayer(playerName);
		if(!unban.message.isEmpty())
		{
			Bukkit.getOfflinePlayer(playerName).setBanned(false);
		}
		{
			if(moderatorName.equals("(Console)"))
			{
				ResponseInterpreter.sendPlayerMessage(moderatorName, ChatColor.GREEN + "\u2554" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + ChatColor.BOLD + "\u25ba");
				ResponseInterpreter.sendPlayerMessage(moderatorName, ChatColor.GREEN + "\u2551" + " " + ChatColor.BOLD + "\u2553" + ChatColor.BOLD + "\u25ba" + ChatColor.RESET  + ChatColor.GREEN + ChatColor.BOLD + "" + ChatColor.ITALIC + "UnBanning Player " + ChatColor.AQUA + "" + ChatColor.BOLD + "" + ChatColor.ITALIC + playerName);
				ResponseInterpreter.sendPlayerMessage(moderatorName, ChatColor.GREEN + "\u2551" + " " + ChatColor.BOLD + "\u2551");
				if(!unban.message.isEmpty())
				{
					ResponseInterpreter.sendPlayerMessage(moderatorName, ChatColor.GREEN + "\u2551" + " " + ChatColor.BOLD + "\u2559" + "\u2500" + "\u2500" + "\u2500" + ChatColor.RESET + "" + ChatColor.GREEN + "\u25ba" + ChatColor.RESET + "" + ChatColor.GREEN + "" + ChatColor.BOLD + "" + ChatColor.ITALIC + 				
							"Message: " +  ChatColor.RESET + ChatColor.GREEN + "" + ChatColor.ITALIC + unban.message);
				}
				else
				{
					ResponseInterpreter.sendPlayerMessage(moderatorName, ChatColor.GREEN + "\u2551" + " " + ChatColor.BOLD + "\u2559" + "\u2500" + "\u2500" + "\u2500" + ChatColor.RESET + "" + ChatColor.GREEN + "\u25ba" + ChatColor.RESET + "" + ChatColor.RED + "" + ChatColor.BOLD + "" + ChatColor.ITALIC + 
							"Error: " +  ChatColor.RESET + ChatColor.GREEN + "" + ChatColor.ITALIC + unban.error);
	
				}
				ResponseInterpreter.sendPlayerMessage(moderatorName, ChatColor.GREEN + "\u2551");
				
				ResponseInterpreter.sendPlayerMessage(moderatorName, ChatColor.GREEN + "\u255a" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + ChatColor.BOLD + "\u25ba");
			}
			else
			{
				if(!unban.message.isEmpty())
				{
					LogSystem.logEvent(LogSystem.LOG_INFO, "Player " + playerName + " unbanned.");
				}
				else
				{
					LogSystem.logEvent(LogSystem.LOG_ERROR, unban.error);
				}
			}
		}
	}

	@Override
	public void run() 
	{
		dealWithUnban();
	}
}
