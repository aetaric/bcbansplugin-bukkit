package jab.bcbans.json;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import jab.bcbans.bukkit.commands.response.ResponseInterpreter;
import jab.bcbans.json.object.Ban;
import jab.bcbans.main.BCBans;
import jab.bcbans.main.LogSystem;

/**
 * Thread Sub-Class of JSONINterpreter dealing with Bans.
 * @author JabJabJab
 *
 */
public class JSONBan extends JSONInterpreter
{
	String reasonBanned = "";

	public JSONBan(String playerName, String moderatorName, String reasonBanned) 
	{
		super(playerName, moderatorName);
		this.reasonBanned = reasonBanned;
	}

	private void dealWithQuery() 
	{
		if(playerName.equals(moderatorName))
		{
			ResponseInterpreter.sendPlayerMessage(moderatorName, "You cannot ban yourself!");
			return;
		}
		Player player = null;
		
		Ban ban = JSONHandler.BanPlayer(playerName, moderatorName, reasonBanned);
		if(!ban.created_at.isEmpty())
		{
			player = Bukkit.getPlayer(playerName);
			if(player == null)
			{
				OfflinePlayer offlinePlayer = Bukkit.getOfflinePlayer(playerName);
				offlinePlayer.setBanned(true);
			}
			else
			{				
				player.setBanned(true);
				player.kickPlayer(BCBans.configuration.getDefaultBanMessage());
			}
		}
		if(!moderatorName.equals("(Console)"))
		{
			
			String messageHeader[] = new String[] 
					{
						ChatColor.GREEN + "\u2554" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + ChatColor.BOLD + "\u25ba",						
						ChatColor.GREEN + "\u2551" + " " + ChatColor.BOLD + "\u2553" + ChatColor.BOLD + "\u25ba" + ChatColor.RESET  + ChatColor.GREEN + ChatColor.BOLD + "" + ChatColor.ITALIC + "Banning Player " + ChatColor.AQUA + "" + ChatColor.BOLD + "" + ChatColor.ITALIC + playerName,
						ChatColor.GREEN + "\u2551" + " " + ChatColor.BOLD + "\u2551"
					};
			ResponseInterpreter.sendPlayerMultipleMessages(moderatorName, messageHeader);
			if(!ban.created_at.isEmpty())
			{
				String messageSuccessful = ChatColor.GREEN + "\u2551" + " " + ChatColor.BOLD + "\u2559" + "\u2500" + "\u2500" + "\u2500" + ChatColor.RESET + "" + ChatColor.GREEN + "\u25ba" + ChatColor.RESET + "" + ChatColor.GREEN + "" + ChatColor.BOLD + "" + ChatColor.ITALIC + 				
						"Message: " +  ChatColor.RESET + ChatColor.GREEN + "" + ChatColor.ITALIC + "Successful ban.";
				ResponseInterpreter.sendPlayerMessage(moderatorName, messageSuccessful);
			}
			else
			{
				String messageFailure = ChatColor.GREEN + "\u2551" + " " + ChatColor.BOLD + "\u2559" + "\u2500" + "\u2500" + "\u2500" + ChatColor.RESET + "" + ChatColor.GREEN + "\u25ba" + ChatColor.RESET + "" + ChatColor.RED + "" + ChatColor.BOLD + "" + ChatColor.ITALIC + 
						"Error: " +  ChatColor.RESET + ChatColor.GREEN + "" + ChatColor.ITALIC + ban.error;
				ResponseInterpreter.sendPlayerMessage(moderatorName, messageFailure);
			}
			ResponseInterpreter.sendPlayerMessage(moderatorName, ChatColor.GREEN + "\u2551");
		
			ResponseInterpreter.sendPlayerMessage(moderatorName, ChatColor.GREEN + "\u255a" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + ChatColor.BOLD + "\u25ba");			
		}
		else
		{
			if(!ban.created_at.isEmpty())
			{				
				LogSystem.logEvent(LogSystem.LOG_INFO,"Banned: " + playerName + ".");
			}
			else
			{
				LogSystem.logEvent(LogSystem.LOG_INFO,"Error: " + ban.error);
			}
		}
		
	}

	@Override
	public void run() 
	{
		dealWithQuery();
	}

}
