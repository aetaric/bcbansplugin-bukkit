package jab.bcbans.json;

import jab.bcbans.configuration.Exemption;
import jab.bcbans.file.RecordFileHandler;
import jab.bcbans.file.RecordTempBan;
import jab.bcbans.json.object.Check;
import jab.bcbans.json.object.Query;
import jab.bcbans.main.BCBans;
import jab.bcbans.main.LogSystem;

import java.io.File;
import java.text.DateFormat;
import java.util.Date;

import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerLoginEvent.Result;

import com.google.gson.Gson;

public class QueryThread extends Thread
{

	private QueryManager queryManager;

	public QueryThread(QueryManager queryManager) 
	{
		this.setName("BCBans Login Query Thread");
		this.queryManager = queryManager;
		
	}
	
	/**
	 * Thread to run the Query Management.
	 */
	@Override
	public void run()
	{
		//While the QueryManager is enabling threads to run...
		Gson gson = new Gson();
		while(!this.queryManager.shouldDisableThreads)
		{
			try
			{
				//If the queue contains players.
				if(this.queryManager.listPlayersLoggingOn.size() > 0)
				{
					//System.out.println("Pulse");
					PlayerLoginEvent playerToBeChecked = null;
					//Gets the first Player object in the queue.
					playerToBeChecked = this.queryManager.listPlayersLoggingOn.get(0);
					this.queryManager.listPlayersLoggingOn.remove(playerToBeChecked);
					Player player = playerToBeChecked.getPlayer();
					if(this.queryManager.listPlayersLoggingOn.contains(playerToBeChecked))
					{
						continue;
					}
					else
					{						
						this.queryManager.listPlayersLoggingOn.add(playerToBeChecked);
					}
					//Removes the Player Object from the list to make sure that all threads do not try to use it.
					this.queryManager.listPlayersLoggingOn.remove(0);
					this.queryManager.listPlayersLoggingOn.trimToSize();
					//Checks player.
					Exemption exemption = this.queryManager.plugin.exemptionList.mapExemptions.get(playerToBeChecked.getPlayer().getName());
					
					//We send the IP address even if they are exempt. We also don't need to check the output as it's unlikely it will fail.
					JSONHandler.PlayerIP(playerToBeChecked.getPlayer().getName(),playerToBeChecked.getAddress().getHostAddress());
					//System.out.println("Pulse2");
					if(exemption != null)
					{
						LogSystem.logEvent(LogSystem.LOG_INFO, "Player Exempt: " + exemption.getExemptionName() + ".");
						
					}
					else
					{
						//Temp-Ban check.
						File tempBanFile = new File(RecordFileHandler.FOLDER_RECORD_TEMPBAN.getAbsolutePath() + player.getName() + ".json");
						//Temp-ban file is there. Doesnt mean that the tempban is still in effect.
						if(tempBanFile.exists())
						{
							Date now = new Date();
							RecordTempBan recordTempBan = gson.fromJson(RecordFileHandler.readJSONFile(tempBanFile), RecordTempBan.class);
							Date timeUnbanned = new Date(recordTempBan.timeBanned);
							//If the temban is still here.
							if(recordTempBan.timeBanned - now.getTime() > 0L)
							{
								LogSystem.logEvent(LogSystem.LOG_INFO, "Player Rejected: " + player.getName() + " is Temp-banned until " + DateFormat.getInstance().format(timeUnbanned));
								playerToBeChecked.disallow(Result.KICK_OTHER,"Test.");
								continue;
							}
						}
						//System.out.println("Pulse3");
						Query query = JSONHandler.QueryPlayer(playerToBeChecked.getPlayer().getName());
						//System.out.println("Pulse4");
						if(query.getGlobalBanCount() > BCBans.configuration.getGlobalBanLimit())
						{
							playerToBeChecked.disallow(Result.KICK_OTHER, BCBans.configuration.getDefaultBanMessage());
							LogSystem.logEvent(LogSystem.LOG_INFO, "PLAYER REJECTED: " + player.getName() + ". Global Bans: " + query.getGlobalBanCount());
							continue;
						}
						else
						{
							//System.out.println("Pulse5");
							Check check = JSONHandler.CheckPlayer(playerToBeChecked.getPlayer().getName());
							//System.out.println("Pulse6");
							if(!check.bans.isEmpty())
							{
								//System.out.println("Pulse6a");
								player.setBanned(true);
								playerToBeChecked.disallow(Result.KICK_FULL, BCBans.configuration.getDefaultBanMessage());
								player.kickPlayer(BCBans.configuration.getDefaultBanMessage());
								LogSystem.logEvent(LogSystem.LOG_INFO, "PLAYER REJECTED: " + player.getName() + ". Global Bans: " + query.getGlobalBanCount());
							}
							else
							{
								//System.out.println("Pulse6b");
								playerToBeChecked.allow();
								LogSystem.logEvent(LogSystem.LOG_INFO, "PLAYER ACCEPTED: " + player.getName() + ". Global Bans: " + query.getGlobalBanCount());
							}
						}
					}
					
				}
			}
			catch(Exception e)
			{
				if(BCBans.configuration.isDebugMode())
				{
					e.printStackTrace();
				}
				else
				{
					try {
						Thread.sleep(100L);
					} catch (InterruptedException e1) {
						e1.printStackTrace();
					}
					continue;
				}
			}
			try {
				Thread.sleep(100L);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
