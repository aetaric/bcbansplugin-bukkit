package jab.bcbans.json;

import org.bukkit.ChatColor;

import jab.bcbans.bukkit.commands.response.ResponseInterpreter;
import jab.bcbans.json.object.Query;

public class JSONQuery extends JSONInterpreter
{
	public boolean basic = true;

	public JSONQuery(String playerName,String moderatorName, boolean isFull) 
	{
		super(playerName,moderatorName);
		basic = !isFull;
		
	}

	private void dealWithQuery() 
	{
		Query query = JSONHandler.QueryPlayer(playerName);
		//if(commandSender instanceof Player)
		{
			int count = 1;
			String aliases = "";
			for(String string : query.alts)
			{
				if(string.contains(playerName))
				{
					count++;
					continue;
				}
				else
				{					
					aliases += string;
					if(count != query.alts.length)
					{
						aliases += ",";
					}
						count++;
				}
				
			}
			ResponseInterpreter.sendPlayerMessage(moderatorName, ChatColor.GREEN + "\u2554" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + ChatColor.BOLD + "\u25ba");
			ResponseInterpreter.sendPlayerMessage(moderatorName, ChatColor.GREEN + "\u2551" + " " + ChatColor.BOLD + "\u2553" + ChatColor.BOLD + "\u25ba" + ChatColor.RESET  + ChatColor.GREEN + ChatColor.BOLD + "" + ChatColor.ITALIC + "Results for " + ChatColor.AQUA + "" + ChatColor.BOLD + "" + ChatColor.ITALIC + playerName);
			ResponseInterpreter.sendPlayerMessage(moderatorName, ChatColor.GREEN + "\u2551" + " " + ChatColor.BOLD + "\u2551");
			ResponseInterpreter.sendPlayerMessage(moderatorName, ChatColor.GREEN + "\u2551" + " " + ChatColor.BOLD + "\u255f" + "\u2500" + "\u2500" + "\u2500" + ChatColor.RESET + "" + ChatColor.GREEN + "\u25ba" + ChatColor.RESET + "" + ChatColor.GREEN + "" + ChatColor.BOLD + "" + ChatColor.ITALIC + 
					"Global Ban Count: " +  ChatColor.RESET + ChatColor.GREEN + "" + ChatColor.ITALIC + query.getGlobalBanCount());
			ResponseInterpreter.sendPlayerMessage(moderatorName, ChatColor.GREEN + "\u2551" + " " + ChatColor.BOLD + "\u2551");
			ResponseInterpreter.sendPlayerMessage(moderatorName, ChatColor.GREEN + "\u2551" + " " + ChatColor.BOLD + "\u2559" + "\u2500" + "\u2500" + "\u2500" + ChatColor.RESET + "" + ChatColor.GREEN + "\u25ba" + ChatColor.RESET + "" + ChatColor.GREEN + "" + ChatColor.BOLD + "" + ChatColor.ITALIC + 
					"Aliases: " + ChatColor.RESET + "" + ChatColor.ITALIC + aliases);
			if(!basic)
			{				
				ResponseInterpreter.sendPlayerMessage(moderatorName, ChatColor.GREEN + "\u2560" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + ChatColor.BOLD + "\u25ba");
				query.processBanInfo(playerName);
			}
			else
			{
				ResponseInterpreter.sendPlayerMessage(moderatorName, ChatColor.GREEN + "\u255a" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + ChatColor.BOLD + "\u25ba");				
			}				
		}
		
	}



	@Override
	public void run() 
	{
		dealWithQuery();
	}
	
	

}
