package jab.bcbans.json;

/**
 * Super Class for JSON Handling.
 * @author JabJabJab
 *
 */
public class JSONInterpreter extends Thread 
{
	/**
	 * String Object to contain the result JSON Text.
	 */
	public String JSON_TEXT = "";
	
	/**
	 * String Object to contain the name of the player being affected by the command execution.
	 */
	public String playerName = "";
	
	/**
	 * CommandSender Object to reference when executing command executions.
	 */
	public String moderatorName = "";
	
	/**
	 * Main Constructor of the JSONInterpreter class. 
	 * This requires the URL Object for JSON requests, the PlayerName String, and the CommandSender sending the command.
	 * @param url
	 * @param playerName
	 * @param sender
	 */
	public JSONInterpreter(String playerName, String moderatorName)
	{
		//Transferring parameters into the fields-\
		this.moderatorName = moderatorName;		//|
		this.playerName = playerName;			//|
		//----------------------------------------/
	}

	
}
