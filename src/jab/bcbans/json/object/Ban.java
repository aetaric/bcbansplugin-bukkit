package jab.bcbans.json.object;

public class Ban 
{
	public String created_at = "";
	public int dispute_id;
	public String updated_at = "";
	public int server_id;
	public int player_id;
	public String issuer = "";
	public int id;
	public String reason = "";
	public boolean global;
	public String error = "";

	public boolean isGlobalBan()
	{
		return global;
	}

	public Ban()
	{
		
	}
}
