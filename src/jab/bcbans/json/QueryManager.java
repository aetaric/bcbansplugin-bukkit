package jab.bcbans.json;

import java.util.ArrayList;

import org.bukkit.event.player.PlayerLoginEvent;

import jab.bcbans.main.BCBans;
import jab.bcbans.main.LogSystem;

public class QueryManager 
{
	public BCBans plugin;
	
	ArrayList<QueryThread> listThreads;
	public ArrayList<PlayerLoginEvent> listPlayersLoggingOn;

	public boolean shouldDisableThreads;
	
	public QueryManager(BCBans bcBans)
	{
		listThreads = new ArrayList<QueryThread>();
		listPlayersLoggingOn = new ArrayList<PlayerLoginEvent>();
		//this.listPlayersLoggingOn
		plugin = bcBans;
		loadThreads();
	}

	private void loadThreads() 
	{
		for(int x = 0; x < BCBans.configuration.getLoginThreadCount(); x++)
		{
			QueryThread thread = new QueryThread(this);
			listThreads.add(thread);
			thread.start();
		}
		LogSystem.logEvent(LogSystem.LOG_INFO, "Loaded " + BCBans.configuration.getLoginThreadCount() + " thread(s) for Processing Logins");
	}
}
