package jab.bcbans.login.listener;

import jab.bcbans.main.BCBans;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;

public class LoginListener implements Listener
{
	protected BCBans plugin;
	
	public LoginListener(BCBans plugin)
	{
		this.plugin = plugin;
	}
	
	/**
     * When a player attempts to log on.
     * @param playerLoggingIn
     */
    @EventHandler
    public void onPlayerLogin(PlayerLoginEvent playerLoggingIn)
    {
    	if(!playerLoggingIn.getPlayer().isBanned())
    	{    		
    		//registers player for querying BCBans.
    		this.RegisterPlayerForAuthentication(playerLoggingIn);
    	}
    }

    /**
     * Registers player for Querying.
     * @param playerLoggingIn
     */
	private void RegisterPlayerForAuthentication(PlayerLoginEvent playerLoggingIn) 
	{
		//adds player to queue list to be queried.
		plugin.queryManager.listPlayersLoggingOn.add(playerLoggingIn);
	}
}
