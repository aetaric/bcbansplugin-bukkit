package jab.bcbans.record.json.ban.temp;

import jab.bcbans.record.json.Record;

/**
 * SubClass of 'Record' That stores recorded data of a tempban.
 * @author JabJabJab
 *
 */
public class RecordTempBan extends Record
{
	/**
	 * Time in milliseconds to where the player is banned.
	 */
	public long timeBanned;
	
	/**
	 * Time measurement that was used in the tempban.
	 */
	public char timeMeasurementUsed;
	
	/**
	 * Constructor used for GSON interpretation
	 */
	public RecordTempBan() {}
}
