package jab.bcbans.record.json;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Scanner;

public class RecordFileHandler 
{
	/**
	 * static File object for main Records folder;
	 */
	public static File FOLDER_RECORD_MAIN 	= new File("plugins/BCBans/Records/");
	
	/**
	 * static File object for Tempban Records folder;
	 */
	public static File FOLDER_RECORD_TEMPBAN 	= new File("plugins/BCBans/Records/TempBan/");
	
	/**
	 * static File object for Ban Records folder;
	 */
	public static File FOLDER_RECORD_BAN 		= new File("plugins/BCBans/Records/Ban/");
	
	/**
	 * static File object for Warn Records folder;
	 */
	public static File FOLDER_RECORD_WARN 	= new File("plugins/BCBans/Records/Warn/");
	
	public static String readJSONFile(File fileRead)
	{
		//Reads file into an ArrayList to be read through.---------------------\
		ArrayList<String> listStrings = new ArrayList<String>();             //
		Scanner scanner = null;                                              //
		try                                                                  //
		{                    												 //
			scanner = new Scanner(new FileInputStream(fileRead), "UTF8");  	 //
		} 																	 //
		catch (FileNotFoundException e) 									 //
		{																	 //
			return "";												 		 //
		}    															     //
	    try 															     //
	    {        															 //
	      while (scanner.hasNextLine()) 									 //
	      {																	 //
	        listStrings.add(scanner.nextLine());							 //
	      }																	 //
	    }																	 //
	    finally																 //
	    {																	 //
	      scanner.close();													 //
	    }																	 //
	    //---------------------------------------------------------------------/
		return listStrings.get(0);	
	}
	
	public static void writeJSONFile(File fileWrite, String json)
	{
		//Writer object declaration.
		Writer out = null;
		try 
		{
			//Opens text.
			out = new OutputStreamWriter(new FileOutputStream(fileWrite), "UTF8");
			
			//Writes text to file in a 'for' loop.
			out.write(json);
		
		} 
		//if something goes wrong:
		catch (IOException e) 
		{
			//Prints stack-trace.
			e.printStackTrace();
		}
	    finally 
	    {
	    	//tries to close the object.
		    try 
		    {
		    	//closes the object.
		    	out.close();
		    }
		    //Catches File In/Out Exception.
		    catch (IOException e) 
		    {
		    	//Prints stack-trace.
				e.printStackTrace();
		    }
	    }
	}

	/**
	 * Creates directories on plug-in initialization.
	 */
	public static void checkDirectories() 
	{
		FOLDER_RECORD_MAIN.mkdirs();
		FOLDER_RECORD_BAN.mkdirs();
		FOLDER_RECORD_TEMPBAN.mkdirs();
		FOLDER_RECORD_WARN.mkdirs();
	}
}
