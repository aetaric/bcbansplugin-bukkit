package jab.bcbans.record.json.punish;

import jab.bcbans.record.json.Record;

public class RecordPunish extends Record
{
	/**
	 * level of punishment.
	 */
	public int punishLevel;
	
	/**
	 * Empty constructor for GSON Interpretation.
	 */
	public RecordPunish() {}
}
