package jab.bcbans.record.json.warn;

import jab.bcbans.record.json.Record;

/**
 * Sub-Class of 'Record' for recording Warnings given using BCBans.
 * @author JabJabJab
 *
 */
public class RecordWarn extends Record
{
	/**
	 * Empty Constructor for GSON interpretation.
	 */
	public RecordWarn() {}
}
