package jab.bcbans.record.json;

import jab.bcbans.main.BCBans;

/**
 * Super class for recording actions done using BCBans.
 * @author JabJabJab
 *
 */
public class Record 
{
	/**
	 * to check if this JSON Document was created when the database is unreachable.
	 */
	public boolean executedOffline = false;
	
	/**
	 * the issuer (Staff Member) to issue the command.
	 */
	public String issuedBy = "";
	
	/**
	 * Player being affected by the execution of the command.
	 */
	public String playerIssued = "";
	
	/**
	 * the Reason for the executed command.
	 */
	public String Reason = "";
	
	/**
	 * Empty Constructor for GSON interpretation.
	 */
	public Record() {}
	
}
