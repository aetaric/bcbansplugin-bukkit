package jab.bcbans.core.json.object;
public class Check 
{
	public String bans = "";
	public String error = "";
	public String created_at = "";
	public int dispute_id = 0;
	public String updated_at = "";
	public int server_id = 0;
	public int player_id = 0;
	public String issuer = "";
	public int id = 0;
	public String reason = "";
	public boolean global = false;
	public boolean isGlobalBan()
	{
		return global;
	}
	public Check() {}
	
	public boolean isBanned()
	{
		return !created_at.isEmpty();
	}
}
