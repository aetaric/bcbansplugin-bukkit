package jab.bcbans.core.json.object;

import jab.bcbans.core.BCCore;

import java.util.ArrayList;

import org.bukkit.ChatColor;

public class Query 
{
	public ArrayList<Ban> bans;
	public String[] alts;
	public String error = "";
	
	public Query()
	{
		
	}
	
	public int getGlobalBanCount()
	{
		int count = 0;
		for(Ban ban: bans)
		{
			if(ban.isGlobalBan())
			{
				count++;
			}
		}
		return count;
	}

	public void processBanInfo(BCCore core, String moderatorName) 
	{
		int count = 1;
		for(Ban ban : bans)
		{
			core.getInterface().messagePlayer(moderatorName, ChatColor.GREEN + "\u2551" + " " + ChatColor.BOLD + "\u2553" + ChatColor.BOLD + "\u25ba" + ChatColor.RESET  + ChatColor.GREEN + ChatColor.BOLD + "" + ChatColor.ITALIC + "Ban (" + count + ")");
			core.getInterface().messagePlayer(moderatorName, ChatColor.GREEN + "\u2551" + " " + ChatColor.BOLD + "\u2551");
			core.getInterface().messagePlayer(moderatorName, ChatColor.GREEN + "\u2551" + " " + ChatColor.BOLD + "\u255f" + "\u2500" + "\u2500" + "\u2500" + ChatColor.RESET + "" + ChatColor.GREEN + "\u25ba" 
					+ ChatColor.RESET + "" + ChatColor.GREEN + "" + ChatColor.BOLD + "" + ChatColor.ITALIC + "Created On: " + ChatColor.RESET + "" + ChatColor.UNDERLINE + "" + ChatColor.GREEN + ban.created_at);
			core.getInterface().messagePlayer(moderatorName, ChatColor.GREEN + "\u2551" + " " + ChatColor.BOLD + "\u2551");
			core.getInterface().messagePlayer(moderatorName, ChatColor.GREEN + "\u2551" + " " + ChatColor.BOLD + "\u255f" + "\u2500" + "\u2500" + "\u2500" + ChatColor.RESET + "" + ChatColor.GREEN + "\u25ba" + ChatColor.RESET + "" + ChatColor.GREEN + "" + ChatColor.BOLD + "" + ChatColor.ITALIC + 
					"Issued By: " + ChatColor.RESET + ChatColor.GREEN + "" + ChatColor.ITALIC + printProperIssuerColor(ban.issuer));
			core.getInterface().messagePlayer(moderatorName, ChatColor.GREEN + "\u2551" + " " + ChatColor.BOLD + "\u2551");
			core.getInterface().messagePlayer(moderatorName, ChatColor.GREEN + "\u2551" + " " + ChatColor.BOLD + "\u255f" + "\u2500" + "\u2500" + "\u2500" + ChatColor.RESET + "" + ChatColor.GREEN + "\u25ba" + ChatColor.RESET + "" + ChatColor.GREEN + "" + ChatColor.BOLD + "" + ChatColor.ITALIC + 
					"Ban Reason: " + ChatColor.RESET + ChatColor.GREEN + "" + ChatColor.ITALIC + ban.reason);
			core.getInterface().messagePlayer(moderatorName, ChatColor.GREEN + "\u2551" + " " + ChatColor.BOLD + "\u2551");
			core.getInterface().messagePlayer(moderatorName, ChatColor.GREEN + "\u2551" + " " + ChatColor.BOLD + "\u2559" + "\u2500" + "\u2500" + "\u2500" + ChatColor.RESET + "" + ChatColor.GREEN + "\u25ba" + ChatColor.RESET + "" + ChatColor.GREEN + "" + ChatColor.BOLD + "" + ChatColor.ITALIC + 
					"Global: " + printProperGlobal(ban.global));
			if(count == bans.size())
			{				
				core.getInterface().messagePlayer(moderatorName, ChatColor.GREEN + "\u255a" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + ChatColor.BOLD + "\u25ba");
			}
			else
			{
				core.getInterface().messagePlayer(moderatorName, ChatColor.GREEN + "\u2560" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + ChatColor.BOLD + "\u25ba");
			}
			count++;
		}
	}

	private String printProperGlobal(boolean global) 
	{
		if(global)
		{
			return new String(ChatColor.RED + "" + ChatColor.BOLD + ChatColor.UNDERLINE + "YES");
		}
		else
		{
			return new String(ChatColor.GREEN + "" + ChatColor.BOLD + "NO");
		}
	}

	private String printProperIssuerColor(String issuer) 
	{
		if(issuer.contains("CONSOLE"))
		{
			return new String(ChatColor.RED + "" + ChatColor.BOLD + "" + ChatColor.ITALIC + issuer);
		}
		else
		{
			return new String(ChatColor.GREEN + "" + ChatColor.ITALIC + issuer);
		}

	}
}
