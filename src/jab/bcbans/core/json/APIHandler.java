package jab.bcbans.core.json;

import jab.bcbans.core.BCCore;

import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

public class APIHandler 
{
	/**
	 * Builds an API URL request to ban a player.
	 * @param playerName
	 * @param sender
	 * @param reason
	 * @return
	 */
	public static URL BAN_PLAYER(String playerName,String moderatorName, String reason)
	{		
		try 
		{
			//Encodes the 'reason' String object to stop any URL-breaking characters from breaking the URL.
			String encodedReason = URLEncoder.encode(reason, "UTF-8");
			
			//Returns the 'URL' Object.
			return new URL("https://www.bcbans.com/api/ban_player?apikey=" + BCCore.configuration.getAPIKey() + "&playername=" + playerName + "&sender=" + moderatorName + "&reason=" + encodedReason);
		}
		
		//Unknown exception:
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * Builds an API URL request to globally ban a player.
	 * @param playerName
	 * @param sender
	 * @param reason
	 * @return
	 */
	public static URL GLOBAL_BAN_PLAYER(String playerName,String moderatorName, String reason)
	{		
		try 
		{		
			//Encodes the 'reason' String object to stop any URL-breaking characters from breaking the URL.
			String encodedReason = URLEncoder.encode(reason, "UTF-8");
			
			//Returns the 'URL' Object.
			return new URL("https://www.bcbans.com/api/ban_player?apikey=" + BCCore.configuration.getAPIKey() + "&playername=" + playerName + "&sender=" + moderatorName + "&reason=" + encodedReason + "&global=1");
		} 
		
		//Unknown exception:
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * Builds an API URL request to check a player.
	 * @param playerName
	 * @return
	 */
	public static URL CHECK_PLAYER(String playerName)
	{		
		try 
		{				
			return new URL("https://www.bcbans.com/api/check?apikey=" + BCCore.configuration.getAPIKey() + "&playername=" + playerName);
		} 
		catch (MalformedURLException e) 
		{			
			e.printStackTrace();
		}
		return null;
	}
	
	public static URL QUERY_PLAYER(String playerName)
	{		
		try 
		{
			return new URL("https://www.bcbans.com/api/query?apikey=" + BCCore.configuration.getAPIKey() + "&playername=" + playerName);
		} 
		catch (MalformedURLException e) 
		{
			if(BCCore.configuration.isDebugMode())
			{				
				e.printStackTrace();
			}
		}
		return null;
	}
	
	public static URL UNBAN_PLAYER(String playerName)
	{		
		try 
		{
				return new URL("https://www.bcbans.com/api/unban_player?apikey=" + BCCore.configuration.getAPIKey() + "&playername=" + playerName);
		} 
		catch (MalformedURLException e) 
		{
			if(BCCore.configuration.isDebugMode())
			{				
				e.printStackTrace();
			}
		}
		return null;
	}
	
	public static URL VERIFY_PLAYER(String playerName,String verificationcode)
	{		
		try 
		{		
				return new URL("https://www.bcbans.com/api/verify?apikey=" + BCCore.configuration.getAPIKey() + "&playername=" + playerName + "&verify=" + verificationcode);
		} 
		catch (MalformedURLException e) 
		{
			if(BCCore.configuration.isDebugMode())
			{				
				e.printStackTrace();
			}
		}
		return null;
	}
	
	public static URL PLAYER_IP(String playerName, String playerIP)
	{	
		try 
		{
			return new URL("https://www.bcbans.com/api/player_ip?apikey=" + BCCore.configuration.getAPIKey() + "&playername=" + playerName+ "&ip=" + isIPLocal(playerIP));
		} 
		catch (MalformedURLException e) 
		{
			if(BCCore.configuration.isDebugMode())
			{				
				e.printStackTrace();
			}
		}
		return null;
	}
	
	public static URL VALIDATE_APIKEY(String API_KEY, String serverSoftware)
	{
		try 
		{
			return new URL("https://www.bcbans.com/api/register?apikey=" + API_KEY + "&version=" + serverSoftware);
		}
		catch (MalformedURLException e) 
		{
			if(BCCore.configuration.isDebugMode())
			{				
				e.printStackTrace();
			}
		}
		return null;
	}
	
	public static String isIPLocal(String hostAddress) 
	{
		if(hostAddress.equals("127.0.0.1"))
		{
			return "";
		}
		else
		{
			return hostAddress;
		}
	}
	
}
