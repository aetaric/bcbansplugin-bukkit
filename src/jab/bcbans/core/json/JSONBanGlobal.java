package jab.bcbans.core.json;

import org.bukkit.ChatColor;

import jab.bcbans.core.BCCore;
import jab.bcbans.core.json.object.Ban;
import jab.bcbans.core.util.LogSystem;

/**
 * 
 * @author JabJabJab
 *
 */
public class JSONBanGlobal extends JSONInterpreter
{
	String reason;
	Ban ban;

	public JSONBanGlobal(BCCore core, String playerName, String moderatorName, String reason, Ban ban) 
	{
		super(core, playerName,moderatorName);
		this.reason = reason;
		this.ban = ban;
	}

	private void dealWithBan() 
	{
		if(playerName.equals(moderatorName))
		{
			core.getInterface().messagePlayer(moderatorName, "You cannot ban yourself!");
			return;
		}

		if(!ban.created_at.isEmpty())
		{
			core.getInterface().kickBanPlayer(playerName, reason);
		}
		if(!moderatorName.equals("(Console)"))
		{
			core.getInterface().messagePlayer(moderatorName, ChatColor.GREEN + "\u2554" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + ChatColor.BOLD + "\u25ba");
			core.getInterface().messagePlayer(moderatorName, ChatColor.GREEN + "\u2551" + " " + ChatColor.BOLD + "\u2553" + ChatColor.BOLD + "\u25ba" + ChatColor.RESET  + ChatColor.GREEN + ChatColor.BOLD + "" + ChatColor.ITALIC + "Globally Banning Player " + ChatColor.AQUA + "" + ChatColor.BOLD + "" + ChatColor.ITALIC + playerName);
			core.getInterface().messagePlayer(moderatorName, ChatColor.GREEN + "\u2551" + " " + ChatColor.BOLD + "\u2551");
			if(!ban.created_at.isEmpty())
			{
				core.getInterface().messagePlayer(moderatorName, ChatColor.GREEN + "\u2551" + " " + ChatColor.BOLD + "\u2559" + "\u2500" + "\u2500" + "\u2500" + ChatColor.RESET + "" + ChatColor.GREEN + "\u25ba" + ChatColor.RESET + "" + ChatColor.GREEN + "" + ChatColor.BOLD + "" + ChatColor.ITALIC + 				
						"Message: " +  ChatColor.RESET + ChatColor.GREEN + "" + ChatColor.ITALIC + "Successful ban.");
			}
			else
			{
				core.getInterface().messagePlayer(moderatorName, ChatColor.GREEN + "\u2551" + " " + ChatColor.BOLD + "\u2559" + "\u2500" + "\u2500" + "\u2500" + ChatColor.RESET + "" + ChatColor.GREEN + "\u25ba" + ChatColor.RESET + "" + ChatColor.RED + "" + ChatColor.BOLD + "" + ChatColor.ITALIC + 
						"Error: " +  ChatColor.RESET + ChatColor.GREEN + "" + ChatColor.ITALIC + ban.error);

			}
			core.getInterface().messagePlayer(moderatorName, ChatColor.GREEN + "\u2551");
			
			core.getInterface().messagePlayer(moderatorName, ChatColor.GREEN + "\u255a" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + ChatColor.BOLD + "\u25ba");			
		}
		else
		{
			if(!ban.created_at.isEmpty())
			{				
				LogSystem.logEvent(LogSystem.LOG_INFO, "Player Globally Banned: " + playerName);
			}
			else
			{
				LogSystem.logEvent(LogSystem.LOG_ERROR, ban.error);
			}
		}
			core.getInterface().kickPlayer(playerName, reason);
	}


	@Override
	public void run() 
	{
		dealWithBan();
	}

}
