package jab.bcbans.core.json;

/**
 * Class to send into the JSON Library primitive data types to keep Bukkit away from the JSON API.
 * @author JabJabJab
 *
 */
public class PlayerLoggingIn 
{
	
	/**
	 * Constructor used to properly input the Player's Name and Player's IP, with Exceptions for invalid entries.
	 * @param playerName
	 * @param playerIP
	 */
	public PlayerLoggingIn(String playerName, String playerIP)
	{
		//If the player's Name is Invalid:
		if(playerName == null || playerName.isEmpty())
		{
			
			//Throw a exception.
			new IllegalArgumentException("Player's name is null or empty!");
		}
		
		//If the player's IP Address is Invalid:
		if(playerIP == null || playerIP.isEmpty())
		{
			
			//Throw a exception.
			new IllegalArgumentException("Player's IP is null or empty!");
		}

		// Casts paramaters to Fields:----\
		this.playerName = playerName;	//|
		this.playerIP = playerIP;		//|
		//--------------------------------/
	}
	
	/**
	 * Player's Name used as their log-in name, not their display nick-name.
	 */
	private String playerName = "";
	
	/**
	 * Player's IP Address used for Internet Identification purposes.
	 */
	private String playerIP = "";
	
	/**
	 * Returns player's name.
	 * @return
	 */
	String getPlayerName()
	{
		
		//Returns Player's name.
		return playerName;
	}
	
	/**
	 * Returns player's IP.
	 * @return
	 */
	String getPlayerIP()
	{
		
		//Returns Player's IP Address.
		return playerIP;
	}
}
