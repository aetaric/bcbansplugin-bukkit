package jab.bcbans.core.json;

import jab.bcbans.core.BCCore;

/**
 * Super Class for JSON Handling.
 * @author JabJabJab
 *
 */
public class JSONInterpreter extends Thread 
{
	/**
	 * String Object to contain the result JSON Text.
	 */
	public String JSON_TEXT = "";
	
	/**
	 * String Object to contain the name of the player being affected by the command execution.
	 */
	public String playerName = "";
	
	/**
	 * CommandSender Object to reference when executing command executions.
	 */
	public String moderatorName = "";
	
	public BCCore core;
	
	/**
	 * Main Constructor of the JSONInterpreter class. 
	 * This requires the URL Object for JSON requests, the PlayerName String, and the CommandSender sending the command.
	 * @param core 
	 * @param url
	 * @param playerName
	 * @param sender
	 */
	public JSONInterpreter(BCCore core, String playerName, String moderatorName)
	{
		//Transferring parameters into the fields-\
		this.core = core;						//|
		this.moderatorName = moderatorName;		//|
		this.playerName = playerName;			//|
		//----------------------------------------/
	}

	
}
