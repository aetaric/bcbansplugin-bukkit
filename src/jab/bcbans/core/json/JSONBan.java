package jab.bcbans.core.json;

import org.bukkit.ChatColor;

import jab.bcbans.core.BCCore;
import jab.bcbans.core.json.object.Ban;
import jab.bcbans.core.util.LogSystem;

/**
 * Thread Sub-Class of JSONINterpreter dealing with Bans.
 * @author JabJabJab
 *
 */
public class JSONBan extends JSONInterpreter
{
	String reasonBanned = "";
	private Ban ban;

	public JSONBan(BCCore core,String playerName, String moderatorName, String reasonBanned, Ban ban) 
	{
		super(core, playerName, moderatorName);
		this.reasonBanned = reasonBanned;
		this.ban = ban;
	}

	private void dealWithQuery() 
	{
		if(playerName.equals(moderatorName))
		{
			core.getInterface().messagePlayer(moderatorName, "You cannot ban yourself!");
			return;
		}
		
		if(!ban.created_at.isEmpty())
		{
			core.getInterface().banPlayer(playerName, reasonBanned);
		}
		if(!moderatorName.equals("(Console)"))
		{
			
			String messageHeader[] = new String[] 
					{
						ChatColor.GREEN + "\u2554" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + ChatColor.BOLD + "\u25ba",						
						ChatColor.GREEN + "\u2551" + " " + ChatColor.BOLD + "\u2553" + ChatColor.BOLD + "\u25ba" + ChatColor.RESET  + ChatColor.GREEN + ChatColor.BOLD + "" + ChatColor.ITALIC + "Banning Player " + ChatColor.AQUA + "" + ChatColor.BOLD + "" + ChatColor.ITALIC + playerName,
						ChatColor.GREEN + "\u2551" + " " + ChatColor.BOLD + "\u2551"
					};
			core.getInterface().messagePlayer(moderatorName, messageHeader);
			if(!ban.created_at.isEmpty())
			{
				String messageSuccessful = ChatColor.GREEN + "\u2551" + " " + ChatColor.BOLD + "\u2559" + "\u2500" + "\u2500" + "\u2500" + ChatColor.RESET + "" + ChatColor.GREEN + "\u25ba" + ChatColor.RESET + "" + ChatColor.GREEN + "" + ChatColor.BOLD + "" + ChatColor.ITALIC + 				
						"Message: " +  ChatColor.RESET + ChatColor.GREEN + "" + ChatColor.ITALIC + "Successful ban.";
				core.getInterface().messagePlayer(moderatorName, messageSuccessful);
			}
			else
			{
				String messageFailure = ChatColor.GREEN + "\u2551" + " " + ChatColor.BOLD + "\u2559" + "\u2500" + "\u2500" + "\u2500" + ChatColor.RESET + "" + ChatColor.GREEN + "\u25ba" + ChatColor.RESET + "" + ChatColor.RED + "" + ChatColor.BOLD + "" + ChatColor.ITALIC + 
						"Error: " +  ChatColor.RESET + ChatColor.GREEN + "" + ChatColor.ITALIC + ban.error;
				core.getInterface().messagePlayer(moderatorName, messageFailure);
			}
			core.getInterface().messagePlayer(moderatorName, ChatColor.GREEN + "\u2551");
		
			core.getInterface().messagePlayer(moderatorName, ChatColor.GREEN + "\u255a" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + ChatColor.BOLD + "\u25ba");			
		}
		else
		{
			if(!ban.created_at.isEmpty())
			{				
				LogSystem.logEvent(LogSystem.LOG_INFO,"Banned: " + playerName + ".");
			}
			else
			{
				LogSystem.logEvent(LogSystem.LOG_INFO,"Error: " + ban.error);
			}
		}
		if(!ban.created_at.isEmpty())
		{
			core.getInterface().kickPlayer(playerName, reasonBanned);
		}
	}

	@Override
	public void run() 
	{
		dealWithQuery();
	}

}
