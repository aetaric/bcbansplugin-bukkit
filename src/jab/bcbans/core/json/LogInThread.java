package jab.bcbans.core.json;

import java.io.File;
import java.text.DateFormat;
import java.util.Date;

import jab.bcbans.core.configuration.Configuration;
import jab.bcbans.core.configuration.Exemption;
import jab.bcbans.core.file.RecordFileHandler;
import jab.bcbans.core.file.RecordTempBan;
import jab.bcbans.core.json.object.Check;
import jab.bcbans.core.json.object.Query;
import jab.bcbans.core.util.LogSystem;

public class LogInThread extends Thread 
{
	
	/**
	 * Integer Object for identifying Threads.
	 */
	public static int THREAD_ID = 0;
	
	/**
	 * Private ThreadManager object to talk back with the Log-in lists of players.
	 */
	private ThreadManager threadManager;
	
	/**
	 * Constructor for LogInThread. adds ThreadManager to properly speak to the management class.
	 * @param threadManager
	 */
	public LogInThread(ThreadManager threadManager)
	{
		//Adds ID variable for proper Thread Object identification.
		THREAD_ID++;
		
		//Sets the name of the Thread for identification.
		this.setName("BCBans - LogInThread #" + THREAD_ID);
		
		//Sets the Field 'threadManager' to the current Management object.
		this.threadManager = threadManager;
	}
	
	public void run()
	{
		//While the QueryManager is enabling threads to run...
		while(pluginEnabled())
		{

			//We will use a 'try-catch' exception to keep our thread running properly, in the event of a potential crash.
			try
			{
				
				//If Players are currently queued to be processed:
				if(ArePlayersWaitingToLogOn())
				{
					//Grabs the next Player queued to log on:
					PlayerLoggingIn playerLoggingIn = getNextPlayerInTheList();
					
					//If the 'PlayerLoggingIn' Object happens to be null:
					if(playerLoggingIn == null)
					{
						
						//Continue to the next While Loop iteration to process another Player.
						continue;
					}
					
					//Else if the 'PlayerLoggingIn' Object is actually a legitimate Object:
					else
					{
						
						//For logging purposes for BCBans, this player's IP will be sent with his or her name to BCBan's database to maintain a healthy IP tracking system.
						LogPlayerIP(playerLoggingIn);
						
						//If the player is exempt from the checking process:
						if(isPlayerExempt(playerLoggingIn))
						{
							
							//Logs the exemption:
							LogSystem.logEvent(LogSystem.LOG_INFO, "Player Exempt: " + playerLoggingIn.getPlayerName() + ".");
						
							//Continues to the next iteration of the thread, letting the PlayerLoggingIn Object go.
							continue;
						}
						
						//Else if the PlayerLoggingIn Object is not exempt, then we move on to the actual checks.
						else
						{
						
							if(isPlayerTemporarilyBanned(playerLoggingIn))
							{
								
								continue;
							}
							
							else
							{
								
								if(isPlayerGlobalBanLimitExceeded(playerLoggingIn))
								{
									
									continue;
								}
								else
								{
									
									if(isPlayerBanned(playerLoggingIn))
									{
										
										continue;
									}
									
									else
									{
										
										LogSystem.logEvent(LogSystem.LOG_INFO, "PLAYER ACCEPTED: " + playerLoggingIn.getPlayerName() + ".");
									}
								}
							}
						}
					}
				}
			}
			
			//Catches any Exception Object and deals with it if we set debug to true.
			catch(Exception e)
			{
				
				//If the Plug-In is in debug-mode, print our Exception.
				if(isDebugModeEnabled())
				{
					
					//Prints exception.
					e.printStackTrace();
				}
			}
			try 
			{
				Thread.sleep(100L);
			}
			catch (InterruptedException e) 
			{
				if(isDebugModeEnabled())
				{
					e.printStackTrace();
				}
			}
		}
	}
	
	private boolean isPlayerBanned(PlayerLoggingIn playerLoggingIn) 
	{
		Check check = threadManager.core.issueCheck(playerLoggingIn.getPlayerName(), "", false);
		boolean isPlayerBanned = check.isBanned();
		if(isPlayerBanned)
		{
			LogSystem.logEvent(LogSystem.LOG_INFO, "PLAYER REJECTED: " + playerLoggingIn.getPlayerName() + ".");
			threadManager.core.getInterface().kickPlayer(playerLoggingIn.getPlayerName(), check.reason);
		}
		else
		{			
		}
		return isPlayerBanned;
	}

	@SuppressWarnings("static-access")
	private boolean isPlayerGlobalBanLimitExceeded(PlayerLoggingIn playerLoggingIn) 
	{
		Query query = threadManager.core.issueQuery(playerLoggingIn.getPlayerName(), "", true);
		boolean isGlobalBanLimitExceeded = query.getGlobalBanCount() > threadManager.core.configuration.getGlobalBanLimit();
		if(isGlobalBanLimitExceeded)
		{
			LogSystem.logEvent(LogSystem.LOG_INFO, "PLAYER REJECTED: " + playerLoggingIn.getPlayerName() + ". Global Bans: " + query.getGlobalBanCount());
			threadManager.core.getInterface().kickPlayer(playerLoggingIn.getPlayerName(), "Global Ban count has been exceeded. Number of Global Bans: " + query.getGlobalBanCount() + ". Global Ban limit for this server: " + threadManager.core.configuration.getGlobalBanLimit() + ".");
		}
		return isGlobalBanLimitExceeded;
	}

	private boolean isPlayerTemporarilyBanned(PlayerLoggingIn playerLoggingIn)
	{
		
		//We grab the potential temp-ban file:
		File tempBanFile = RecordTempBan.getTempBanRecord(playerLoggingIn.getPlayerName());
		
		//If this file even exists:
		if(tempBanFile.exists())
		{
			
			//We grab the JSON object of this ban:
			RecordTempBan recordTempBan = JSONHandler.gson.fromJson(RecordFileHandler.readJSONFile(tempBanFile), RecordTempBan.class);
			
			//We make a new Date object for the time right now.
			Date now = new Date();
			
			//We then make a Date object that reperesents the time that the player is unbanned from the temporary ban.
			Date timeUnbanned = new Date(recordTempBan.timeBanned);
			//If the temban is still here.
			if(recordTempBan.timeBanned - now.getTime() > 0L)
			{
				
				//Log the temporary ban access denied message:
				LogSystem.logEvent(LogSystem.LOG_INFO, "Player Rejected: " + playerLoggingIn.getPlayerName() + " is Temp-banned until " + DateFormat.getInstance().format(timeUnbanned));
				
				//disconnect the player attempting to log in.
				threadManager.core.getInterface().kickPlayer(playerLoggingIn.getPlayerName(), recordTempBan.Reason);
				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}

	private boolean isPlayerExempt(PlayerLoggingIn playerLoggingIn) 
	{
		
		Exemption exemption = threadManager.core.exemptionList.mapExemptions.get(playerLoggingIn);
		
		//We send the IP address even if they are exempt. We also don't need to check the output as it's unlikely it will fail.
		if(exemption != null)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	private void LogPlayerIP(PlayerLoggingIn playerLoggingIn) 
	{

		//Logs Player.
		JSONHandler.PlayerIP(playerLoggingIn.getPlayerName(),playerLoggingIn.getPlayerName());
		
	}

	/**
	 * Method that returns a 'PlayerLoggingIn' object from the list of people queued.
	 * @return
	 */
	public PlayerLoggingIn getNextPlayerInTheList() 
	{
		
		//Grabs the first 'PlayerLoggingIn' on the list:
		PlayerLoggingIn playerLoggingIn = threadManager.listPlayersLoggingOn.get(0);
		
		//Removes the 'PlayerLoggingIn' from the list:
		threadManager.listPlayersLoggingOn.remove(playerLoggingIn);
		
		//If the 'PlayerLoggingIn' is already being processed by another 'LogInThread' Object:
		if(threadManager.listPlayersLoggingOn.contains(playerLoggingIn))
		{
			
			//Returns null to be handled by the 'LogInThread' Object properly.
			return null;
		}
		
		//If this 'PlayerLoggingIn' Object is not being processed by any other 'LogInThread' Object:
		else
		{			
			
			//Add the 'PlayerLoggingIn' Object properly.
			threadManager.listPlayersLoggingOn.add(playerLoggingIn);
			
			//Remove 'PlayerLoggingIn' Object from the Queue list:
			threadManager.removePlayerLogginInFromList(playerLoggingIn);
			
			//Return the 'PlayerLoggingIn' Object to the 'LogInThread' Object to be processed.
			return playerLoggingIn;
		}
	}

	/**
	 * Returns whether or not the Thread Object should keep running.
	 * @return
	 */
	public boolean pluginEnabled()
	{
		
		//Returns thread Disable boolean:
		return !threadManager.shouldDisableThreads;
	}
	
	/**
	 * Returns if the Plug-In is in debug mode.
	 * @return
	 */
	public boolean isDebugModeEnabled()
	{
		
		//Returns if DebugMode is enabled:
		return Configuration.isDebugMode;
	}
	
	/**
	 * Returns if PlayerLoggingIn Objects are queued.
	 * @return
	 */
	public boolean ArePlayersWaitingToLogOn()
	{
		
		//Returns if player(s) are on the queue to be checked:
		return threadManager.listPlayersLoggingOn.size() > 0;
	}
	
}
