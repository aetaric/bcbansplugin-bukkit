package jab.bcbans.core.json;

import jab.bcbans.core.BCCore;
import jab.bcbans.core.json.object.Ban;
import jab.bcbans.core.json.object.Check;
import jab.bcbans.core.json.object.PlayerIP;
import jab.bcbans.core.json.object.Query;
import jab.bcbans.core.json.object.UnBan;
import jab.bcbans.core.json.object.Verify;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;

import com.google.gson.Gson;

/**
 * This class is made to convert URL to JSON String.
 * @author JabJabJab
 *
 */
public class JSONHandler 
{
	/**
	 * GSON object to use globally to prevent multiple objects floating around for no reason.
	 */
	public static Gson gson = new Gson();
	
	/**
	 * Returns a Ban Object while processing a Ban.
	 * @param playerName
	 * @param sender
	 * @param reason
	 * @return
	 */
	public static Ban BanPlayer(String playerName,String moderatorName, String reason)
	{
		//Creates URL using API:
		URL url = APIHandler.BAN_PLAYER(playerName,moderatorName,reason);
		
		//Returns construct of the Ban Object result:
		return JSONHandler.gson.fromJson(JSONHandler.URLtoJSON(url), Ban.class);
	}
	
	/**
	 * Returns a Ban Object while processing a Ban.
	 * @param playerName
	 * @param sender
	 * @param reason
	 * @return
	 */
	public static Ban GlobalBanPlayer(String playerName,String moderatorName, String reason)
	{
		//Creates URL using API:
		URL url = APIHandler.GLOBAL_BAN_PLAYER(playerName,moderatorName,reason);
		//Returns construct of the Ban Object result:
		return JSONHandler.gson.fromJson(JSONHandler.URLtoJSON(url), Ban.class);
	}
	
	/**
	 * Returns a Check Object while processing a Ban.
	 * @param playerName
	 * @return
	 */
	public static Check CheckPlayer(String playerName)
	{
		//Creates URL using API:
		URL url = APIHandler.CHECK_PLAYER(playerName);
		//Returns construct of the Check Object result:
		return JSONHandler.gson.fromJson(JSONHandler.URLtoJSON(url), Check.class);
	}
	
	public static Query QueryPlayer(String playerName) 
	{
		//Creates URL using API:
		URL url = APIHandler.QUERY_PLAYER(playerName);
		
		//Returns construct of the Check Object result:
		return JSONHandler.gson.fromJson(JSONHandler.URLtoJSON(url), Query.class);
	}
	
	public static UnBan UnBanPlayer(String playerName) 
	{
		//Creates URL using API:
		URL url = APIHandler.UNBAN_PLAYER(playerName);
		
		//Returns construct of the Check Object result:
		return JSONHandler.gson.fromJson(JSONHandler.URLtoJSON(url), UnBan.class);
	}
	
	public static Verify VerifyPlayer(String playerName, String verificationCode) 
	{
		//Creates URL using API:
		URL url = APIHandler.VERIFY_PLAYER(playerName,verificationCode);
		
		//Returns construct of the Check Object result:
		return JSONHandler.gson.fromJson(JSONHandler.URLtoJSON(url), Verify.class);
	}
	
	public static void PlayerIP(String playerName, String playerIP) 
	{
		//Creates URL using API:
		URL url = APIHandler.PLAYER_IP(playerName, playerIP);
		
		//Returns construct of the Check Object result:
		JSONHandler.gson.fromJson(JSONHandler.URLtoJSON(url), PlayerIP.class);
	}
	
	public static String ValidateAPIKey(String API_KEY, String serverSoftware) 
	{
		//Creates URL using API:
		URL url = APIHandler.VALIDATE_APIKEY(API_KEY,serverSoftware);
		
		//Returns JSON directly:
		return JSONHandler.URLtoJSON(url);
	}

	/**
	 * Reads URL response.
	 * @param url
	 * @return
	 */
	public static String URLtoJSON(URL url)
	{
		//Final String Object of JSON;
		final String finalJSON;
		
		//If the URL object is not defined:
		if(url == null)
		{
			//All hell breaks loose:
			throw new IllegalArgumentException("URL OBJECT IS NULL!");
		}
		
		//else if everything is okay:
		else
		{
			//Sets up a standard 'URL' try catch setup:
			try 
			{
				//Opens the stream using the URL object.
				InputStream is = url.openStream();
				
				//Creats a reader Object for the stream provided:
				BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
				
				//Reads the entire stream and compiles it into a string.
				finalJSON = readAll(rd);
				return finalJSON;
			}
			catch (MalformedURLException e)
			{
				if(BCCore.configuration.isDebugMode())
				{				
					e.printStackTrace();
				}
			}
			catch (IOException e)
			{
				if(BCCore.configuration.isDebugMode())
				{	
					e.printStackTrace();
				}
			}	
		}
		return "";
	}

	/**
	 * Method that creates a 'String' object from all the Characters.
	 * @param rd
	 * @return
	 * @throws IOException
	 */
	 private static String readAll(Reader rd) throws IOException 
	  {
	    StringBuilder sb = new StringBuilder();
	    int cp;
	    while ((cp = rd.read()) != -1) 
	    {
	      sb.append((char) cp);
	    }
	    return sb.toString();
	  }
}
