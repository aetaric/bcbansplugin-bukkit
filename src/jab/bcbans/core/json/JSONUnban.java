package jab.bcbans.core.json;

import org.bukkit.ChatColor;

import jab.bcbans.core.BCCore;
import jab.bcbans.core.json.object.UnBan;
import jab.bcbans.core.util.LogSystem;

public class JSONUnban extends JSONInterpreter
{
	
	UnBan unBan;
	
	public JSONUnban(BCCore core, String playerName, String moderatorName, UnBan unBan) 
	{
		super(core,playerName,moderatorName);
		this.unBan = unBan;
	}

	private void dealWithUnban() 
	{
		if(unBan.error.isEmpty())
		{
			core.getInterface().unBanPlayer(playerName);
		}
		{
			if(!moderatorName.equals("(Console)"))
			{
				core.getInterface().messagePlayer(moderatorName, ChatColor.GREEN + "\u2554" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + ChatColor.BOLD + "\u25ba");
				core.getInterface().messagePlayer(moderatorName, ChatColor.GREEN + "\u2551" + " " + ChatColor.BOLD + "\u2553" + ChatColor.BOLD + "\u25ba" + ChatColor.RESET  + ChatColor.GREEN + ChatColor.BOLD + "" + ChatColor.ITALIC + "UnBanning Player " + ChatColor.AQUA + "" + ChatColor.BOLD + "" + ChatColor.ITALIC + playerName);
				core.getInterface().messagePlayer(moderatorName, ChatColor.GREEN + "\u2551" + " " + ChatColor.BOLD + "\u2551");
				if(unBan.error.isEmpty())
				{
					core.getInterface().messagePlayer(moderatorName, ChatColor.GREEN + "\u2551" + " " + ChatColor.BOLD + "\u2559" + "\u2500" + "\u2500" + "\u2500" + ChatColor.RESET + "" + ChatColor.GREEN + "\u25ba" + ChatColor.RESET + "" + ChatColor.GREEN + "" + ChatColor.BOLD + "" + ChatColor.ITALIC + 				
							"Message: " +  ChatColor.RESET + ChatColor.GREEN + "" + ChatColor.ITALIC + unBan.message);
				}
				else
				{
					core.getInterface().messagePlayer(moderatorName, ChatColor.GREEN + "\u2551" + " " + ChatColor.BOLD + "\u2559" + "\u2500" + "\u2500" + "\u2500" + ChatColor.RESET + "" + ChatColor.GREEN + "\u25ba" + ChatColor.RESET + "" + ChatColor.RED + "" + ChatColor.BOLD + "" + ChatColor.ITALIC + 
							"Error: " +  ChatColor.RESET + ChatColor.GREEN + "" + ChatColor.ITALIC + unBan.error);
	
				}
				core.getInterface().messagePlayer(moderatorName, ChatColor.GREEN + "\u2551");
				
				core.getInterface().messagePlayer(moderatorName, ChatColor.GREEN + "\u255a" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + ChatColor.BOLD + "\u25ba");
			}
			else
			{
				if(unBan.error.isEmpty())
				{
					LogSystem.logEvent(LogSystem.LOG_INFO, "Player " + playerName + " unbanned.");
				}
				else
				{
					LogSystem.logEvent(LogSystem.LOG_ERROR, unBan.error);
				}
			}
		}
	}

	@Override
	public void run() 
	{
		dealWithUnban();
	}
}
