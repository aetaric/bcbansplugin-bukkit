package jab.bcbans.core.json;

import java.util.ArrayList;

import jab.bcbans.core.BCCore;
import jab.bcbans.core.configuration.Configuration;
import jab.bcbans.core.util.LogSystem;

public class ThreadManager 
{
	
	public BCCore core;
	public ArrayList<LogInThread> listThreads;
	public ArrayList<PlayerLoggingIn> listPlayersLoggingOn;

	public boolean shouldDisableThreads = false;
	
	public ThreadManager(BCCore core)
	{
		this.core = core;
		listThreads = new ArrayList<LogInThread>();
		listPlayersLoggingOn = new ArrayList<PlayerLoggingIn>();
		//this.listPlayersLoggingOn
		loadThreads();
	}

	private void loadThreads() 
	{
		for(int x = 0; x < Configuration.getLoginThreadCount(); x++)
		{
			LogInThread thread = new LogInThread(this);
			listThreads.add(thread);
			thread.start();
		}
		LogSystem.logEvent(LogSystem.LOG_INFO, "Loaded " + Configuration.getLoginThreadCount() + " thread(s) for Processing Logins");
	}
	
	/**
	 * Properly removes a 'PlayerLoggingIn' Object from the lists.
	 * @param playerLoggingIn
	 */
	public void removePlayerLogginInFromList(PlayerLoggingIn playerLoggingIn)
	{
		
		//Removes the Player Object from the list to make sure that all threads do not try to use it.
		listPlayersLoggingOn.remove(0);
		
		//Trims list:
		listPlayersLoggingOn.trimToSize();
	}
}
