package jab.bcbans.core.json;


import org.bukkit.ChatColor;

import jab.bcbans.core.BCCore;
import jab.bcbans.core.json.object.Verify;

public class JSONVerify extends JSONInterpreter
{
	Verify verify;
	
	public JSONVerify(BCCore core, String playerName,Verify verify) 
	{
		super(core, playerName,"none");
		this.verify = verify;
	}

	private void dealWithQuery() 
	{

			if(verify.error.isEmpty())
			{
				core.getInterface().messagePlayer(playerName, ChatColor.GREEN + "\u2554" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + ChatColor.BOLD + "\u25ba");
				core.getInterface().messagePlayer(playerName, ChatColor.GREEN + "\u2551" + " " + ChatColor.BOLD + "\u2553" + ChatColor.BOLD + "\u25ba" + ChatColor.RESET  + ChatColor.GREEN + ChatColor.BOLD + "" + ChatColor.ITALIC + "Verifying Player " + ChatColor.AQUA + "" + ChatColor.BOLD + "" + ChatColor.ITALIC + playerName);
				core.getInterface().messagePlayer(playerName, ChatColor.GREEN + "\u2551" + " " + ChatColor.BOLD + "\u2551");
				core.getInterface().messagePlayer(playerName, ChatColor.GREEN + "\u2551" + " " + ChatColor.BOLD + "\u2559" + "\u2500" + "\u2500" + "\u2500" + ChatColor.RESET + "" + ChatColor.GREEN + "\u25ba" + ChatColor.RESET + "" + ChatColor.GREEN + "" + ChatColor.BOLD + "" + ChatColor.ITALIC + 				
						"Verification: " +  ChatColor.RESET + ChatColor.RED + "" + ChatColor.ITALIC + "Successful");
				core.getInterface().messagePlayer(playerName, ChatColor.GREEN + "\u2551");	
				core.getInterface().messagePlayer(playerName, ChatColor.GREEN + "\u255a" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + ChatColor.BOLD + "\u25ba");
			}
			else
			{
				core.getInterface().messagePlayer(playerName, ChatColor.GREEN + "\u2554" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + ChatColor.BOLD + "\u25ba");
				core.getInterface().messagePlayer(playerName, ChatColor.GREEN + "\u2551" + " " + ChatColor.BOLD + "\u2553" + ChatColor.BOLD + "\u25ba" + ChatColor.RESET  + ChatColor.GREEN + ChatColor.BOLD + "" + ChatColor.ITALIC + "Verifying Player " + ChatColor.AQUA + "" + ChatColor.BOLD + "" + ChatColor.ITALIC + playerName);
				core.getInterface().messagePlayer(playerName, ChatColor.GREEN + "\u2551" + " " + ChatColor.BOLD + "\u2551");
				core.getInterface().messagePlayer(playerName, ChatColor.GREEN + "\u2551" + " " + ChatColor.BOLD + "\u2559" + "\u2500" + "\u2500" + "\u2500" + ChatColor.RESET + "" + ChatColor.GREEN + "\u25ba" + ChatColor.RESET + "" + ChatColor.GREEN + "" + ChatColor.BOLD + "" + ChatColor.ITALIC + 				
						"Error: " +  ChatColor.RESET + ChatColor.RED + "" + ChatColor.ITALIC + verify.error);
				core.getInterface().messagePlayer(playerName, ChatColor.GREEN + "\u2551");	
				core.getInterface().messagePlayer(playerName, ChatColor.GREEN + "\u255a" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + ChatColor.BOLD + "\u25ba");
		}		
	}

	@Override
	public void run() 
	{
		dealWithQuery();
	}
	
	

}
