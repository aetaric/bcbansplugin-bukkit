package jab.bcbans.core.json;

import org.bukkit.ChatColor;

import jab.bcbans.core.BCCore;
import jab.bcbans.core.json.object.Query;

public class JSONQuery extends JSONInterpreter
{
	public boolean basic = true;
	Query query;

	public JSONQuery(BCCore core, String playerName,String moderatorName, boolean isBasic, Query query) 
	{
		super(core,playerName,moderatorName);
		this.query = query;
		basic = isBasic;
		
	}

	private void dealWithQuery() 
	{
		//if(commandSender instanceof Player)
		{
			int count = 1;
			String aliases = "";
			for(String string : query.alts)
			{
				if(string.contains(playerName))
				{
					count++;
					continue;
				}
				else
				{					
					aliases += string;
					if(count != query.alts.length)
					{
						aliases += ",";
					}
						count++;
				}
				
			}
			core.getInterface().messagePlayer(moderatorName, ChatColor.GREEN + "\u2554" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + ChatColor.BOLD + "\u25ba");
			core.getInterface().messagePlayer(moderatorName, ChatColor.GREEN + "\u2551" + " " + ChatColor.BOLD + "\u2553" + ChatColor.BOLD + "\u25ba" + ChatColor.RESET  + ChatColor.GREEN + ChatColor.BOLD + "" + ChatColor.ITALIC + "Results for " + ChatColor.AQUA + "" + ChatColor.BOLD + "" + ChatColor.ITALIC + playerName);
			core.getInterface().messagePlayer(moderatorName, ChatColor.GREEN + "\u2551" + " " + ChatColor.BOLD + "\u2551");
			core.getInterface().messagePlayer(moderatorName, ChatColor.GREEN + "\u2551" + " " + ChatColor.BOLD + "\u255f" + "\u2500" + "\u2500" + "\u2500" + ChatColor.RESET + "" + ChatColor.GREEN + "\u25ba" + ChatColor.RESET + "" + ChatColor.GREEN + "" + ChatColor.BOLD + "" + ChatColor.ITALIC + 
					"Global Ban Count: " +  ChatColor.RESET + ChatColor.GREEN + "" + ChatColor.ITALIC + query.getGlobalBanCount());
			core.getInterface().messagePlayer(moderatorName, ChatColor.GREEN + "\u2551" + " " + ChatColor.BOLD + "\u2551");
			core.getInterface().messagePlayer(moderatorName, ChatColor.GREEN + "\u2551" + " " + ChatColor.BOLD + "\u2559" + "\u2500" + "\u2500" + "\u2500" + ChatColor.RESET + "" + ChatColor.GREEN + "\u25ba" + ChatColor.RESET + "" + ChatColor.GREEN + "" + ChatColor.BOLD + "" + ChatColor.ITALIC + 
					"Aliases: " + ChatColor.RESET + "" + ChatColor.ITALIC + aliases);
			if(!basic)
			{				
				core.getInterface().messagePlayer(moderatorName, ChatColor.GREEN + "\u2560" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + ChatColor.BOLD + "\u25ba");
				query.processBanInfo(core, moderatorName);
			}
			else
			{
				core.getInterface().messagePlayer(moderatorName, ChatColor.GREEN + "\u255a" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + ChatColor.BOLD + "\u25ba");				
			}				
		}
		
	}



	@Override
	public void run() 
	{
		dealWithQuery();
	}
	
	

}
