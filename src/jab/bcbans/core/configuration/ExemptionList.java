package jab.bcbans.core.configuration;

import java.io.File;
import java.net.URL;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import com.google.gson.Gson;

import jab.bcbans.core.BCCore;
import jab.bcbans.core.file.FileHandler;
import jab.bcbans.core.json.APIHandler;
import jab.bcbans.core.json.JSONHandler;
import jab.bcbans.core.json.object.Check;

/**
 * A class designed to keep order to the list of exemptions for the BCBans plug-in.
 * @author JabJabJab
 *
 */
public class ExemptionList 
{
	
	/**
	 * HashMap object with a playerName as the 'String' Key, and the 'Exemption' object as the stored object.
	 */
	public HashMap<String,Exemption> mapExemptions;
	
	/**
	 * Exemption configuration file.
	 */
	File exemptionsFile = new File("plugins/BCBans/Bcbans-exemptions.txt");
	
	/**
	 * Object of the Main Class object reference for this class to function properly.
	 */
	BCCore core;
	
	/**
	 * Constructor for the class which requires the BCBans object.
	 * @param bcCore
	 */
	public ExemptionList(BCCore core) 
	{
		//Transfers BCCore object to our local object.
		this.core = core;
	
		//Imports the exceptions that already exist.
		importExemptionsFile();
	}

	/**
	 * Imports the exceptions from the configuration file for them.
	 */
	private void importExemptionsFile() 
	{
		//If the 'File' Object exists:
		if(exemptionsFile.exists())
		{
			readExemptionsFile();
		}
		//If the 'File' Object does NOT exist:
		else
		{
			FileHandler.writeFile(exemptionsFile, factoryExemption);
		}
	}

	/**
	 * Reads and distributes 'Exemption' objects and their definitions accordingly.
	 */
	private void readExemptionsFile() 
	{
		//Declares the HashMap Object storing the exemptions.
		mapExemptions = new HashMap<String,Exemption>();
		
		//reads through the exemptions configuration file:
		ArrayList<String> listExemptions = FileHandler.readFile(exemptionsFile);

		//Goes through each line in the exemptions configuration file:
		for(String lineExemption : listExemptions)
		{
			//If a line starts with a "#" (a commented line):
			if(lineExemption.startsWith("#"))
			{
				//Continues to the next line.
				continue;
			}
			
			//Else, if this is a line that should be processed:
			else
	    	{
				//Creates a 'Exemption' Object for processing data.
				Exemption exemption = new Exemption();
				
				//If this line does not contain a ':', read as only a name.
				if(!lineExemption.contains(":"))
	    		{
					//sets Exemption name.
					exemption.setExemptionName(lineExemption);

					//places Exemption object into the list of Exceptions.
					mapExemptions.put(exemption.getExemptionName(), exemption);
					
					//Continues onto the next line of Exemptions.
	    			continue;
	    		}
	    		else
	    		{
	    			//This try is here for one reason: If an exemption has improper syntax, then we will simply ignore it and go about our day. 
	    			try
	    			{
	    				//Splits string by ':' to contain
		    			String[] stringArray = lineExemption.split(":");
		    			
		    			//Loops through the split raw 'String' array result:
		    			for(int splitStringArrayDisplacement = 0; splitStringArrayDisplacement < stringArray.length; splitStringArrayDisplacement++)
		    			{
		    				//Sets Exemption object definitions:------------------\
		    				if(splitStringArrayDisplacement == 0)				//|
		    				{													//|
		    					exemption.setExemptionName(stringArray[0]);		//|
		    				}													//|
		    				else if(splitStringArrayDisplacement == 1)			//|
		    				{													//|
		    					exemption.setExemptionReason(stringArray[1]);	//|
		    				}													//|
		    				else if(splitStringArrayDisplacement == 2)			//|
		    				{													//|
		    					exemption.setExemptionDate(stringArray[2]);		//|
		    				}													//|
		    				else if(splitStringArrayDisplacement > 2)			//|
		    				{													//|
		    					break;											//|
		    				}													//|
		    				//----------------------------------------------------/
		    			}
	    			}
		    		catch(Exception e)
		    		{
		    			//Ignores error and continues..
		    			continue;
		    		}
	    			//Places finished Exemption object into the list.
	    			mapExemptions.put(exemption.getExemptionName(), exemption);
	    		}
	    	}
		}
	}

	/**
	 * Refreshes the exemptions list by dumping the old exemptions and re-importing the file.
	 */
	public void refreshExemptions() 
	{	
		//Re-Imports all the HashMap files.
		importExemptionsFile();
	}

	/**
	 * Removes the exemption of a player using a 'String' object.
	 * @param name - Player name.
	 * @return - if the execution is successful.
	 */
	public boolean removeException(String name) 
	{
		//If the name actually isn't in the list of Keys of the Exemption List (aka the actual person's name as a 'String' object):
		if(!mapExemptions.containsKey(name))
		{
			//Return false and go about your day...
			return false;
		}
		
		//Tries to perform the removal of the exemption:
		try
		{
			//Removes from the Exemptions list:
			mapExemptions.remove(name);
			
			//Write the exemption:
			saveExemptionList();
		
			//Creates a GSon generated object:----------------\
			Gson gson = new Gson();							//|
			URL url = APIHandler.CHECK_PLAYER(name);		//|
			String json = JSONHandler.URLtoJSON(url);		//|
			Check check = gson.fromJson(json, Check.class); //|
			//------------------------------------------------/
			
			//Is the player banned from the server?
			if(check.isBanned())
			{
				//if so, then we get the Player object from the Bukkit API:
				Player player = Bukkit.getPlayer(name);
				
				//If this person is not our imaginary friend, we kick him, because he is not important you see.
				if(player != null)
				{
					//Bye-bye. :)
					player.kickPlayer(BCCore.configuration.getDefaultBanMessage() + ChatColor.RED + " (Exception Removed)");
				}
			}
			return true;
		}
		//Catches the exception.
		catch(Exception e)
		{
			if(BCCore.configuration.isDebugMode())
			{
				e.printStackTrace();
			}
			return false;
		}
		
	}

	/**
	 * Saves the Exemption list to the file.
	 */
	public void saveExemptionList()
	{
		//Creates a String ArrayList to convert the new Exemptions list:
		ArrayList<String> listExemptionsModified = new ArrayList<String>();
		
		//Converts ExemptionList into ArrayList<String>:
		for (Exemption exemptionFromList : mapExemptions.values()) 
		{
			//adds exceptions to the ArrayList.
		    listExemptionsModified.add(new String(exemptionFromList.getExemptionName() + ':' + exemptionFromList.getExemptionReason() + ':' + exemptionFromList.getExemptionDate() + System.getProperty("line.separator")));
		}
	}
	
	/**
	 * Adds the exemption to the exemptionList.
	 * @param playerName
	 * @param reason
	 * @return
	 */
	public boolean addException(String playerName,String reason) 
	{
		//If the player is already on the Exemption list:
		if(mapExemptions.containsKey(playerName))
		{
			//Nothing to be done here. Return false.
			return false;
		}
		else
		{
			try
			{				
				//New Exemption object:
				Exemption exemption = new Exemption();
				
				//Create the Time String:-----------------------------\
				Date date = new Date();								//|
				Format formatter = new SimpleDateFormat("MM/dd/yy");//|
				//---------------------------------------------------//
				//
				//Sets Exemption data accordingly:------------------------\
				exemption.setExemptionDate(formatter.format(date));		//|
				exemption.setExemptionName(playerName);					//|
				exemption.setExemptionReason(reason);					//|
				//Places new Exemption Object into the List:			//|
				this.mapExemptions.put(playerName, exemption);			//|
				//--------------------------------------------------------/
				
				//Writes the exemption out.
				saveExemptionList();				
			}
			
			//Catches exception:
			catch(Exception e)
			{
				if(BCCore.configuration.isDebugMode())
				{
					e.printStackTrace();
				}
				return false;				
			}
			return true;
		}
		
		
	}
	
	/**
	 * Adds exemption to the exemptionList without the reason.
	 * @param name
	 * @return
	 */
	public boolean addException(String name) 
	{
		return addException(name,"no reason");
	}
	
	/**
	 * String for the default text when creating the Exemption file.
	 */
	String[] factoryExemption = new String[]
	{
		"#For Each Line, place a name to be exempt from BCBans system, then the reason, and then the date, or just the name (WITH NO SPACES!!!)." + System.getProperty("line.separator"),
		"#For example: Jab:FormerGreifer:1/11/2011" + System.getProperty("line.separator"),
		"#Or         : Jab"
	};
}
