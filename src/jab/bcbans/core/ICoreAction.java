package jab.bcbans.core;

import java.util.Date;

/**
 * Interface class to handle outside server method handling to keep the BCCore neat and clean.
 * @author JabJabJab
 *
 */
public interface ICoreAction 
{
	
	/**
	 * Interface method to handle a event kickPlayer, with a message String passed.
	 * @param playerName
	 * @param message
	 */
	public void kickPlayer(String playerName, String message);
	
	/**
	 * Interface method to handle a event banPlayer, with a message String passed.
	 * @param playerName
	 * @param message
	 */
	public void banPlayer(String playerName, String message);
	
	/**
	 * Interface method to handle a event unBanPlayer.
	 * @param playerName
	 */
	public void unBanPlayer(String playerName);
	
	/**
	 * Interface method to handle a event warnPlayer, with a message String passed.
	 * @param playerName
	 * @param message
	 */
	public void warnPlayer(String playerName, String message);
	
	/**
	 * Interface method to handle a event messagePlayer, with a message String passed.
	 * @param playerName
	 * @param message
	 */
	public void messagePlayer(String playerName, String message);
	
	/**
	 * Interface method to handle a event warnPlayer, with a raw String Array of messages passed.
	 * @param playerName
	 * @param messages
	 */
	public void messagePlayer(String playerName, String[] messages);
	
	/**
	 * Interface method to handle a event kickBanPlayer, with a message String passed. 
	 * @param playerName
	 * @param message
	 */
	public void kickBanPlayer(String playerName, String message);
	
	/**
	 * Interface method to handle a event notifyPlayerLoggingIn, with a message String passed.
	 * @param playerName
	 * @param message
	 */
	public void notifyPlayerLoggingIn(String playerName, String message);
	
	public void notifyTempBanPlayer(String playerName, String message, Date dateUnBanned);
	
	/**
	 * Interface method to handle BCCore enabling.
	 */
	public void onCoreEnable();
	
	/**
	 * Interface method to handle BCCore disabling.
	 */
	public void onCoreDisable();
	
	
}
