package jab.bcbans.core.util;

import jab.bcbans.core.BCCore;

import java.util.logging.Logger;

public class LogSystem 
{
	BCCore plugin;
	public static final String LOG_INFO = "INFO";	
	public static final String LOG_URGENT = "URGENT";
	public static final String LOG_FATAL = "FATAL";
	public static final String LOG_ERROR = "ERROR";	

	static Logger log;
	
	public static void logEvent(String event, String message)
	{
		String compiledMessage = "" + event + " : " + message + ".";

		if(log == null)
		{
			System.out.println("BCBans : " + compiledMessage);
		}
		else
		{			
			log.info(compiledMessage);
		}
	}
	
	public static void setLogger(Logger logger)
	{
		log = logger;
	}
}
