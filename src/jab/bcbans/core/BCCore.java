package jab.bcbans.core;

import jab.bcbans.core.configuration.Configuration;
import jab.bcbans.core.configuration.ExemptionList;
import jab.bcbans.core.json.JSONBan;
import jab.bcbans.core.json.JSONBanGlobal;
import jab.bcbans.core.json.JSONCheck;
import jab.bcbans.core.json.JSONHandler;
import jab.bcbans.core.json.JSONQuery;
import jab.bcbans.core.json.JSONUnban;
import jab.bcbans.core.json.JSONVerify;
import jab.bcbans.core.json.PlayerLoggingIn;
import jab.bcbans.core.json.ThreadManager;
import jab.bcbans.core.json.object.Ban;
import jab.bcbans.core.json.object.Check;
import jab.bcbans.core.json.object.Query;
import jab.bcbans.core.json.object.UnBan;
import jab.bcbans.core.json.object.Verify;
import jab.bcbans.core.util.LogSystem;

import java.io.File;
import java.util.logging.Logger;


/**
 * This is the main class of the BCBans Plug-In API. This handles all general operation of the tool.
 * @author JabJabJab
 *
 */
public class BCCore 
{

	ICoreAction actionInterface;
	
	/**
	 * ExemptionsList Object for handling exemptions.
	 */
	public ExemptionList exemptionList;
	
	/**
	 * Configuration Object for core configurations.
	 */
	public static Configuration configuration;
	
	/**
	 * LogSystem Object for forwarding Logger Object usage.
	 */
	public LogSystem logSystem;
	
	/**
	 * ThreadManager Object for handling the management of LogInThread Objects.
	 */
	public ThreadManager threadManager;
	
	public boolean isCoreEnabled = false;
	
	private static BCCore core;
	
	/**
	 * Main Constructor requiring a 'File' Object for the Configuration file location.
	 * @param fileConfiguration
	 */
	public BCCore(File fileConfiguration, String ServerSoftware)
	{
		
		//Sets Configuration File to one specified in parameters, and loads Configurations.
		configuration = new Configuration(fileConfiguration);
		
		//Initializes exception list:
		exemptionList = new ExemptionList(this);
		
		//Registers the API with IP.
		RegisterAPI(ServerSoftware);
		core = this;
	}
	
	/**
	 * Registers the APIKey to the BCBans database.
	 * @param serverSoftware
	 */
	private void RegisterAPI(String serverSoftware) 
	{
		
		//Creates a JSON result string from attempting to register the APIKey:
		String validationResult = JSONHandler.ValidateAPIKey(configuration.getAPIKey(),serverSoftware);
		
		//If this APIKey is invalid:
		if (validationResult.contains("invalid apikey"))
		{
			
			//Alerts console:
			LogSystem.logEvent(LogSystem.LOG_ERROR, "Your apikey is invalid. Please check your config file! BCBans is now disabled!");
			
			//Disables the LogInThread system.
			disableCore();
		}
	}
	
	/**
	 * Disables the LogInThread system.
	 */
	public static void disableCore() 
	{
		if(core.isCoreEnabled)
		{			
			//Sets boolean to true to alert threads to close.
			core.threadManager.shouldDisableThreads = true;
			for(Thread thread : core.threadManager.listThreads)
			{
				thread.interrupt();
			}
		}
		
	}
	
	/**
	 * Enables the LogInThread system.
	 */
	public void enableCore()
	{
		
		//if the core is ready to enable:
		if(isCoreReadyToEnable())
		{			
			
			//creates new Manager to start the LogInThread system again.
			threadManager = new ThreadManager(this);
			
			//changes Enabled boolean to true to show to the world our plug-in is now enabled.
			isCoreEnabled = true;
		}
		else
		{
			
			//Logs the issue to the console.
			LogSystem.logEvent(LogSystem.LOG_ERROR, "The BCCore has not been properly configured! Check your code.");
			
			//Ensures the plug-in notifies it is disabled when requested.
			isCoreEnabled = false;
		}
	}

	/**
	 * Boolean method to check if required setup has been made prior to enabling the plug-in.
	 * @return
	 */
	private boolean isCoreReadyToEnable() 
	{
		
		//Our lovely boolean to use throughout all the checks.
		boolean isCoreReadyToEnable = true;
		
		//Has the plug-in shell given us a Interface to interact with the server software?
		if(actionInterface == null)
		{
			
			//Notify the boolean that the Core is not ready yet.
			isCoreReadyToEnable = false;
		}
		
		//returns whether or not the Core is ready to enable.
		return isCoreReadyToEnable;
	}

	/**
	 * Sets the Logger Object for our LogSystem for BCCore to one specified.
	 * @param log
	 */
	public void setLogger(Logger log)
	{
		
		//Sets the Logger object in LogSystem:
		LogSystem.setLogger(log);		
	}
	
	/**
	 * Sets the proper Interface Object to handle Server events needing handling.
	 * @param actionInterface
	 */
	public void setInterface(ICoreAction actionInterface)
	{
		this.actionInterface = actionInterface;
	}
	
	/**
	 * Returns the Interface object.
	 * @return
	 */
	public ICoreAction getInterface()
	{
		return actionInterface;
	}

	/**
	 * Adds a playerLoggingIn object to be checked.
	 * @param playerLoggingIn
	 */
	public void checkPlayer(PlayerLoggingIn playerLoggingIn) 
	{
		threadManager.listPlayersLoggingOn.add(playerLoggingIn);
	}
	
	public Query issueQuery(String playerName, String moderatorName, boolean isFull)
	{
		//We create the JSON Result object here:--------------\
		Query query = JSONHandler.QueryPlayer(playerName);	//|
		//----------------------------------------------------/
		
		//We then create the JSON 'Thread' Object and run it to process the result on the server: ----\
		JSONQuery jsonQuery = new JSONQuery(this, playerName, moderatorName, isFull, query);		//|
		jsonQuery.run();																			//|
		//--------------------------------------------------------------------------------------------/
		
		//Return the 'Query' Object just in-case someone wanted to further analyze the result check.
		return query;
	}
	
	/**
	 * Returns a Ban Object after executing a ban on a player using the BCCore system.
	 * @param playerName
	 * @param moderatorName
	 * @param reason
	 * @return
	 */
	public Ban issueBan(String playerName, String moderatorName, String reason)
	{
		//We create the JSON Result object here:------------------------------\
		Ban ban = JSONHandler.BanPlayer(playerName,moderatorName,reason);	//|
		//--------------------------------------------------------------------/
		
		//We then create the JSON 'Thread' Object and run it to process the result on the server: ----\
		JSONBan jsonBan = new JSONBan(this, playerName, moderatorName,reason, ban);					//|
		jsonBan.run();																				//|
		//--------------------------------------------------------------------------------------------/
		
		//Return the 'Ban' Object just in-case someone wanted to further analyze the result check.
		return ban;
	}
	
	public Ban issueGlobalBan(String playerName, String moderatorName, String reason)
	{
		//We create the JSON Result object here:----------------------------------\
		Ban ban = JSONHandler.GlobalBanPlayer(playerName,moderatorName,reason);	//|
		//------------------------------------------------------------------------/
		
		//We then create the JSON 'Thread' Object and run it to process the result on the server: --------\
		JSONBanGlobal jsonBanGlobal = new JSONBanGlobal(this, playerName, moderatorName,reason, ban);	//|
		jsonBanGlobal.run();																			//|
		//------------------------------------------------------------------------------------------------/
		
		//Return the 'Ban' Object just in-case someone wanted to further analyze the result check.
		return ban;
	}
	
	/**
	 * Returns a UnBan Object after executing a check on a player using the BCCore system.
	 * @param playerName
	 * @param moderatorName
	 * @return
	 */
	public UnBan issueUnBan(String playerName, String moderatorName)
	{
		//We create the JSON Result object here:--------------\
		UnBan unBan = JSONHandler.UnBanPlayer(playerName);	//|
		//----------------------------------------------------/
		
		//We then create the JSON 'Thread' Object and run it to process the result on the server: ----\
		JSONUnban jsonUnBan = new JSONUnban(this, playerName, moderatorName, unBan);				//|
		jsonUnBan.run();																			//|
		//--------------------------------------------------------------------------------------------/
		
		//Return the 'UnBan' Object just in-case someone wanted to further analyze the result check.
		return unBan;
	}
	
	/**
	 * Returns a Check Object after executing a check on a player using the BCCore system.
	 * @param playerName
	 * @param moderatorName
	 * @param isBasic
	 * @return
	 */
	public Check issueCheck(String playerName, String moderatorName, boolean isBasic)
	{
		LogSystem.logEvent(LogSystem.LOG_INFO, "Moderator " + moderatorName + " is requesting a Check on player " + playerName);
		//We create the JSON Result object here:--------------\
		Check check = JSONHandler.CheckPlayer(playerName);	//|
		//----------------------------------------------------/
		
		//We then create the JSON 'Thread' Object and run it to process the result on the server: ----\
		JSONCheck jsonCheck = new JSONCheck(this, playerName, moderatorName, check, !isBasic);		//|
		jsonCheck.run();																			//|
		//--------------------------------------------------------------------------------------------/
		
		//Return the 'Check' Object just in-case someone wanted to further analyze the result check.
		return check;
	}
	
	public Verify issueVerify(String playerName, String verificationCode)
	{
		//We create the JSON Result object here:----------------------------------\
		Verify verify = JSONHandler.VerifyPlayer(playerName, verificationCode);	//|
		//------------------------------------------------------------------------/
		
		//We then create the JSON 'Thread' Object and run it to process the result on the server: ----\
		JSONVerify jsonVerify = new JSONVerify(this, playerName, verify);							//|
		jsonVerify.run();																			//|
		//--------------------------------------------------------------------------------------------/
		
		//Return the 'Verify' Object just in-case someone wanted to further analyze the result check.
		return verify;
	}
	
	/*public Warn warnPlayer(String playerName, String moderatorName, String message)
	{
		
	}*/
	
	/**
	 * Adds an exemption to the exemptionList.
	 * @param player
	 * @param reason
	 */
	public boolean addExemption(BCPlayer player, String reason)
	{
		return this.exemptionList.addException(player.getPlayerName(), reason);
	}
	
	/**
	 * Removes an exemption to the exemptionList.
	 * @param player
	 */
	public boolean removeExemption(BCPlayer player)
	{
		return this.exemptionList.removeException(player.getPlayerName());
	}
	
	/**
	 * Refreshes the ExemptionList cache data.
	 */
	public void refreshExemptions()
	{
		this.exemptionList.refreshExemptions();
	}
}
