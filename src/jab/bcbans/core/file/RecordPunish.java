package jab.bcbans.core.file;


public class RecordPunish extends Record
{
	/**
	 * level of punishment.
	 */
	public int punishLevel;
	
	/**
	 * Empty constructor for GSON Interpretation.
	 */
	public RecordPunish() {}
}
