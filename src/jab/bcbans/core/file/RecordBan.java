package jab.bcbans.core.file;

import java.io.File;


/**
 * Sub-Class of 'Record' For recording Bans for BCBans.
 * @author JabJabJab
 *
 */
public class RecordBan extends Record
{
	/**
	 * Empty Constructor for GSON interpretation.
	 */
	public RecordBan() {}
	
	public static File getTempBanRecord(String playerName)
	{
		return new File(RecordFileHandler.FOLDER_RECORD_BAN.getAbsolutePath() + playerName + ".json");
	}
}
