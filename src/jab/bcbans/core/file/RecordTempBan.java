package jab.bcbans.core.file;

import java.io.File;


/**
 * SubClass of 'Record' That stores recorded data of a tempban.
 * @author JabJabJab
 *
 */
public class RecordTempBan extends Record
{
	/**
	 * Time in milliseconds to where the player is banned.
	 */
	public long timeBanned;
	
	/**
	 * Constructor used for GSON interpretation
	 */
	public RecordTempBan() {}
	
	/**
	 * Returns a File Object of a json tempban record
	 * @param playerName
	 * @return File Object of a json tempban record
	 */
	public static File getTempBanRecord(String playerName)
	{
		return new File(RecordFileHandler.FOLDER_RECORD_TEMPBAN.getAbsolutePath() + playerName + ".json");
	}
}
