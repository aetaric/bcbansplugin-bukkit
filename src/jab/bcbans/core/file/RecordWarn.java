package jab.bcbans.core.file;


/**
 * Sub-Class of 'Record' for recording Warnings given using BCBans.
 * @author JabJabJab
 *
 */
public class RecordWarn extends Record
{
	/**
	 * Empty Constructor for GSON interpretation.
	 */
	public RecordWarn() {}
}
