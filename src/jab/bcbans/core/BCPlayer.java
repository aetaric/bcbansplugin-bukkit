package jab.bcbans.core;

/**
 * Shell class of Player to contain only primitives independent of the Bukkit library.
 * @author JabJabJab
 *
 */
public class BCPlayer 
{
	/**
	 * 'String' Object holding the Player's Name.
	 */
	private String playerName = "";
	
	/**
	 * 'String' Object holding the Player's INetAddress.
	 */
	private String playerIP = "";
	
	/**
	 * Constructor for 'BCPlayer' class. Needs to have PlayerName and IP as parameters.
	 * @param playerName
	 * @param playerIP
	 */
	public BCPlayer(String playerName, String playerIP)
	{
		//Sets the Parameters to fields:--\
		this.playerName = playerName;	//|
		this.playerIP = playerIP;		//|
		//--------------------------------/
	}
	
	/**
	 * Returns Player's name as a 'String' Object.
	 * @return
	 */
	public String getPlayerName()
	{
		return playerName;
	}
	
	/**
	 * Returns Player's IP Address as a 'String' Object.
	 * @return
	 */
	public String getPlayerIP()
	{
		return playerIP;
	}
	
}
