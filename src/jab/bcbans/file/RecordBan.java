package jab.bcbans.file;


/**
 * Sub-Class of 'Record' For recording Bans for BCBans.
 * @author JabJabJab
 *
 */
public class RecordBan extends Record
{
	/**
	 * Empty Constructor for GSON interpretation.
	 */
	public RecordBan() {}
}
