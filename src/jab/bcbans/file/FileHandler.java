package jab.bcbans.file;

import jab.bcbans.main.BCBans;
import jab.bcbans.main.LogSystem;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Class to read and write to files using String ArrayList, or raw String Arrays.
 * @author JabJabJab
 *
 */
public class FileHandler 
{
	
	/**
	 * Method that returns an ArrayList<String> for lines read.
	 * @param fileRead
	 * @return
	 */
	public static ArrayList<String> readFile(File fileRead)
	{
		//Reads file into an ArrayList to be read through.---------------------\
		ArrayList<String> listStrings = new ArrayList<String>();             //
		Scanner scanner = null;                                              //
		try                                                                  //
		{                    												 //
			scanner = new Scanner(new FileInputStream(fileRead), "UTF8");  	 //
		} 																	 //
		catch (FileNotFoundException e) 									 //
		{																	 //
			return listStrings;												 //
		}    															     //
	    try 															     //
	    {        															 //
	      while (scanner.hasNextLine()) 									 //
	      {																	 //
	        listStrings.add(scanner.nextLine());							 //
	      }																	 //
	    }																	 //
	    finally																 //
	    {																	 //
	      scanner.close();													 //
	    }																	 //
	    //---------------------------------------------------------------------/
		return listStrings;	
	}
	
	/**
	 * Writes a file using a raw 'String' array.
	 * @param file
	 * @param stringArray
	 */
	public static void writeFile(File file, String[] stringArray)
	{
		//Creates ArrayList object containing String datatype.
		ArrayList<String> arrayListString = new ArrayList<String>();
		
		//Converts the raw 'String' array into the ArrayList<String> object:
		for(int stringArrayPlacement = 0; stringArrayPlacement < stringArray.length; stringArrayPlacement++)
		{
			arrayListString.add(stringArray[stringArrayPlacement]);
		}
		
		//Calls the 'WriteFile' method with the ArrayList<String> Object duplicate.
		writeFile(file,arrayListString);
	}
	
	/**
	 * Writes a file.
	 * @param file
	 * @param fileContent
	 */
	public static void writeFile(File file, ArrayList<String> fileContent)
	{
		//Writer object declaration.
		Writer out = null;
		try 
		{
			//Opens text.
			out = new OutputStreamWriter(new FileOutputStream(file), "UTF8");
			
			//Writes text to file in a 'for' loop.-------------------------------------\
			for(int x = 0; x < fileContent.size(); x++)								 //|
			{																		 //|
				out.write(fileContent.get(x) + System.getProperty("line.separator"));//|
			}																		 //|
			//-------------------------------------------------------------------------/
		} 
		//if something goes wrong:
		catch (IOException e) 
		{
			//Disables plug-in.
			LogSystem.logEvent(LogSystem.LOG_FATAL, "Error creating file: " + file.getName() + " in " + file.getAbsolutePath() + " .");
			BCBans.disablePlugin();
			//Prints stack-trace.
			e.printStackTrace();
		}
	    finally 
	    {
	    	//tries to close the object.
		    try 
		    {
		    	//closes the object.
		    	out.close();
		    }
		    //Catches File In/Out Exception.
		    catch (IOException e) 
		    {
		    	//Prints stack-trace.
				e.printStackTrace();
		    }
	    }
	}
}
