package jab.bcbans.bukkit;

import jab.bcbans.bukkit.commands.BCBan;
import jab.bcbans.bukkit.commands.BCBanGlobal;
import jab.bcbans.bukkit.commands.BCCheck;
import jab.bcbans.bukkit.commands.BCExempt;
import jab.bcbans.bukkit.commands.BCListWarns;
import jab.bcbans.bukkit.commands.BCQuery;
import jab.bcbans.bukkit.commands.BCTempban;
import jab.bcbans.bukkit.commands.BCUnban;
import jab.bcbans.bukkit.commands.BCVerify;
import jab.bcbans.bukkit.commands.BCWarn;

public class CommandList 
{
	public BCQuery bcQuery;
	public BCBan bcBan;
	public BCUnban bcUnban;
	public BCBanGlobal bcBanGlobal;
	public BCCheck bcCheck;
	public BCVerify bcVerify;
	public BCExempt bcExempt;
	public BCWarn bcWarn;
	public BCListWarns bcListWarns;
	public BCTempban bcTempban;
	@SuppressWarnings("unused")
	private BukkitPlugIn plugIn;
	
	public CommandList(BukkitPlugIn plugIn)
	{
		this.plugIn = plugIn;
		bcQuery = new BCQuery(plugIn);
		plugIn.getCommand("BCquery").setExecutor(bcQuery);
		bcBan = new BCBan(plugIn);
		plugIn.getCommand("BCban").setExecutor(bcBan);
		BCUnban unban = new BCUnban(plugIn);
		plugIn.getCommand("BCunban").setExecutor(unban);
		bcBanGlobal = new BCBanGlobal(plugIn);
		plugIn.getCommand("BCbanglobal").setExecutor(bcBanGlobal);
		bcCheck = new BCCheck(plugIn);
		plugIn.getCommand("BCCheck").setExecutor(bcCheck);
		bcVerify = new BCVerify(plugIn);
		plugIn.getCommand("BCverify").setExecutor(bcVerify);
		bcExempt = new BCExempt(plugIn);
		plugIn.getCommand("BCExempt").setExecutor(bcExempt);
		bcWarn = new BCWarn(plugIn);
		plugIn.getCommand("BCwarn").setExecutor(bcWarn);
		bcListWarns = new BCListWarns(plugIn);
		plugIn.getCommand("BCwarns").setExecutor(bcListWarns);
		bcTempban = new BCTempban(plugIn);
		plugIn.getCommand("BCtempban").setExecutor(bcTempban);
	}
}
