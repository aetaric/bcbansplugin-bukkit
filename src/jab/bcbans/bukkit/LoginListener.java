package jab.bcbans.bukkit;


import jab.bcbans.core.json.PlayerLoggingIn;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;

public class LoginListener implements Listener
{
	private BukkitPlugIn plugIn;
	
	public LoginListener(BukkitPlugIn plugIn)
	{
		this.plugIn = plugIn;
	}
	
	/**
     * When a player attempts to log on.
     * @param playerLoggingIn
     */
    @EventHandler
    public void onPlayerLogin(PlayerLoginEvent playerLoggingIn)
    {
    	if(!playerLoggingIn.getPlayer().isBanned())
    	{    		
    		//registers player for querying BCBans.
    		this.RegisterPlayerForAuthentication(playerLoggingIn);
    	}
    }

    /**
     * Registers player for Querying.
     * @param playerLoggingIn
     */
	private void RegisterPlayerForAuthentication(PlayerLoginEvent playerLoginEvent) 
	{
		//Converts the Bukkit Object for PlayerLoginEvent to BCBans 'PlayerLoggingIn' object, with only what we need.
		PlayerLoggingIn playerLoggingIn = new PlayerLoggingIn(playerLoginEvent.getPlayer().getName(),playerLoginEvent.getAddress().getHostAddress());
		
		//adds player to queue list to be queried.
		plugIn.core.checkPlayer(playerLoggingIn);
	}
}
