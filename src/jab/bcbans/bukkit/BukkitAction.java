package jab.bcbans.bukkit;

import java.text.DateFormat;
import java.util.Date;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import jab.bcbans.core.ICoreAction;

public class BukkitAction implements ICoreAction
{

	public void kickPlayer(String playerName, String message) 
	{
		Player player = Bukkit.getPlayerExact(playerName);
		player.kickPlayer(message);
	}

	public void banPlayer(String playerName, String message) 
	{
		
	}

	public void unBanPlayer(String playerName) 
	{
		
	}

	public void warnPlayer(String playerName, String message) 
	{
		
	}

	public void messagePlayer(String playerName, String message) 
	{
		if(!playerName.equals(""))
		{			
			Player playerToMessage = Bukkit.getPlayerExact(playerName);
			if(playerToMessage == null)
			{
				return;
			}
			if(playerToMessage.isOnline())
			{
				playerToMessage.sendMessage(message);
			}		
		}
	}

	public void messagePlayer(String playerName, String[] messages) 
	{
		if(!playerName.equals(""))
		{
			Player playerToMessage = Bukkit.getPlayerExact(playerName);
			if(playerToMessage == null)
			{
				return;
			}
			if(playerToMessage.isOnline())
			{
				playerToMessage.sendMessage(messages);
			}
		}
	}

	public void kickBanPlayer(String playerName, String message) 
	{
		
	}

	public void notifyPlayerLoggingIn(String playerName, String message) 
	{
		
	}

	public void onCoreEnable() 
	{
		
	}

	public void onCoreDisable() 
	{
		
	}

	public void notifyTempBanPlayer(String playerName, String message, Date dateUnBanned) 
	{
		
		//Creates message String to send to player on kick:
		String screenMessage = "Temporary Ban" + "\n" + ChatColor.GREEN + "\"" + message + "\"" + "\n\n" + ChatColor.WHITE + "You are TempBanned until: " + "\n" + ChatColor.GREEN + ChatColor.ITALIC + DateFormat.getInstance().format(dateUnBanned);
		
		//Kicks player with generated message:
		Bukkit.getPlayer(playerName).kickPlayer(screenMessage);
	}

}
