package jab.bcbans.bukkit.commands;

import jab.bcbans.bukkit.BukkitPlugIn;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class BCVerify extends BCCommand implements CommandExecutor
{

	public BCVerify(BukkitPlugIn plugIn) 
	{
		super(plugIn);
	}

	public boolean onCommand(CommandSender sender, Command command, String label, String[] field) 
	{	
		Player player = null;
		if (sender instanceof Player) 
		{
			player = (Player) sender;
		}
		if(player != null)
		{
			if(field.length > 0)
			{
				plugIn.core.issueVerify(getSenderName(sender), field[0]);
				return true;				
			}
		}
		return false;
	}

}
