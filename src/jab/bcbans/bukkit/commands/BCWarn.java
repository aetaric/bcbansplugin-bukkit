package jab.bcbans.bukkit.commands;

import java.io.File;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;

import jab.bcbans.bukkit.BukkitPlugIn;
import jab.bcbans.core.file.FileHandler;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Class for Command execution of the /BCWarn command via Bukkit.
 * @author JabJabJab
 *
 */
public class BCWarn extends BCCommand implements CommandExecutor
{

	public BCWarn(BukkitPlugIn plugIn) 
	{
		super(plugIn);
	}

	public boolean onCommand(CommandSender sender, Command command, String label, String[] field) 
	{
		Player player = null;
		if (sender instanceof Player) 
		{
			//Cast sender as a Player object.
			player = (Player) sender;
		}
		if(isAbleToUseCommand(sender))
		{	
			boolean doesWarnFolderExist = true;
			if(field.length >= 2)
			{
				String playerName = field[0];
				String message = field[1];
				for(int x = 2; x < field.length; x++)
				{
					message += " " + field[x];
				}
				sender.sendMessage(ChatColor.BOLD + "" + ChatColor.ITALIC + "" + ChatColor.GREEN + "Warning Player " + playerName + ".");
				//Warns player
				//
				//If plugin folder does not exist yet:
				if(!this.plugIn.getDataFolder().exists())
				{
					//Makes plugin folder.
					this.plugIn.getDataFolder().mkdirs();
				}
				//Creates 'String' link to the Warns folder we will address when writing warn data.
				String warnFolderString = this.plugIn.getDataFolder().toString() + "/Warns/";
				//Creates 'File' object of the Warns folder.
				File warnFolder = new File(warnFolderString);
				//If the folder does not exist:
				if(!warnFolder.exists())
				{
					//Creates the folder.
					warnFolder.mkdirs();
					//notifies process that the folder did not exist, to not check for previous warns of the player.
					doesWarnFolderExist = false;
				}
				
				//checking previous warns of the player
				File warnTextDocument = new File(warnFolderString + playerName + ".txt");
				ArrayList<String> warnFileText = FileHandler.readFile(warnTextDocument);;
				if(doesWarnFolderExist)
				{
					Date date = new Date();
					warnFileText.add(message + " - " + sender.getName() + " on " + DateFormat.getInstance().format(date));
					FileHandler.writeFile(warnTextDocument, warnFileText);
				}
				player.sendMessage(new String[]{"","","",ChatColor.RED + "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~",""});
				player.sendMessage(ChatColor.RED +                       "                      ~((MODERATOR WARNING))~");
				player.sendMessage(new String[]{"",""});
				player.sendMessage(ChatColor.DARK_RED + "--> " + ChatColor.RED  +                       message                                                  );
				player.sendMessage(new String[]{"","",ChatColor.RED + "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~",""});
				sender.sendMessage("Player " + playerName + " now has " + warnFileText.size() + " warnings.");
				return true;
			}
		}
		return false;
	}

}
