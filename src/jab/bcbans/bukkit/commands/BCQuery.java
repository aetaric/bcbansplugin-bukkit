package jab.bcbans.bukkit.commands;

import jab.bcbans.bukkit.BukkitPlugIn;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class BCQuery extends BCCommand implements CommandExecutor
{

	public BCQuery(BukkitPlugIn plugIn) 
	{
		super(plugIn);
	}

	public boolean onCommand(CommandSender sender, Command command, String label, String[] field) 
	{
		if(isAbleToUseCommand(sender))
		{			
			String playerName = field[0];
			boolean basic = true;
			if(field.length > 1)
			{				
				if(field[1].toLowerCase().equals("full"))
				{
					basic = false;
				}
			}
			
			plugIn.core.issueQuery(playerName, getSenderName(sender), !basic);
			return true;
		}
		return false;
	}

}
