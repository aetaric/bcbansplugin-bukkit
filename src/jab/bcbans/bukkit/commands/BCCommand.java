package jab.bcbans.bukkit.commands;


import jab.bcbans.bukkit.BukkitPlugIn;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * This class is a SuperClass designed for commands to be able to use redundant calls through this class.
 * @author JabJabJab
 *
 */
public class BCCommand 
{
	/**
	 * Object for the Main Class to reference when needed for this class, as well as it's subclasses.
	 */
	public BukkitPlugIn plugIn;

	/**
	 * Constructor for Command Super-Class.
	 * @param bcBans
	 */
	public BCCommand(BukkitPlugIn plugIn)
	{
		//sets Plug-in object to BCBans main Object:
		this.plugIn = plugIn;
	}
	
	/**
	 * Method to determine whether or not this person is able to use the command requested to be executed.
	 * @param sender
	 * @return
	 */
	public boolean isAbleToUseCommand(CommandSender sender)
	{
		//boolean to determine if the player can use this command.
		boolean canUseCommand = false;
		Player player = null;
		if (sender instanceof Player) 
		{
			//Cast sender as a Player object.
			player = (Player) sender;
		}
		//If this is a console command request:
		if(player == null)
		{
			return true;
		}
		
		//if the player null, and if this server is using permissionsex:
		else if(BukkitPlugIn.pex != null && player != null)
		{
			//if permissionsex allows this player to use the command:
			if(BukkitPlugIn.pex.has(player, "bcbans.ban.local"))
			{
				canUseCommand = true;
			}
		}
		//if the local Operators configurations file has this player in it: 
		if(player.isOp())
		{
			canUseCommand = true; 
		}
		return canUseCommand;
	}
	
	/**
	 * Method that returns "(Console)" for a nullified Sending name.
	 * @param sender - command sender.
	 * @return - Appropriate name.
	 */
	public String getSenderName(CommandSender sender) 
	{
		//If this is from the console:
		if(sender == null)
		{
			 return "(CONSOLE)";
		}
		//If this name is from in-game:
		else
		{
			return sender.getName();
		}

	}
}
