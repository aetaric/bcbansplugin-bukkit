package jab.bcbans.bukkit.commands;

import jab.bcbans.bukkit.BukkitPlugIn;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class BCBan extends BCCommand implements CommandExecutor
{

	public BCBan(BukkitPlugIn plugIn) 
	{
		super(plugIn);
	}

	public boolean onCommand(CommandSender sender, Command command, String label, String[] field) 
	{
		if(isAbleToUseCommand(sender))
		{			
			if(field.length >= 2)
			{
				String playerName = field[0];
				String reason = field[1];
				for(int x = 2; x < field.length; x++)
				{
					reason += " " + field[x];
				}
				sender.sendMessage(ChatColor.BOLD + "" + ChatColor.ITALIC + "" + ChatColor.GREEN + "Banning Player...");
				
				plugIn.core.issueGlobalBan(playerName, getSenderName(sender), reason);
				return true;
			}
		}
		return false;
	}

}
