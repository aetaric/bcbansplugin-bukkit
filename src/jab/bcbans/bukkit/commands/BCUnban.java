package jab.bcbans.bukkit.commands;

import jab.bcbans.bukkit.BukkitPlugIn;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class BCUnban extends BCCommand implements CommandExecutor
{

	public BCUnban(BukkitPlugIn plugIn) 
	{
		super(plugIn);
	}

	public boolean onCommand(CommandSender sender, Command command, String label, String[] field) 
	{
		if(isAbleToUseCommand(sender))
		{			
			if(field.length >= 1)
			{
				sender.sendMessage(ChatColor.BOLD + "" + ChatColor.ITALIC + "" + ChatColor.GREEN + "UnBanning Player...");
				plugIn.core.issueUnBan(field[0], getSenderName(sender));
				return true;
			}
		}
		return false;
	}

}
