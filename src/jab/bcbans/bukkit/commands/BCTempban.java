package jab.bcbans.bukkit.commands;

import java.io.File;
import java.text.DateFormat;
import java.util.Date;

import jab.bcbans.bukkit.BukkitPlugIn;
import jab.bcbans.core.file.RecordFileHandler;
import jab.bcbans.core.file.RecordTempBan;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import com.google.gson.Gson;

public class BCTempban extends BCCommand implements CommandExecutor
{

	public BCTempban(BukkitPlugIn plugIn) 
	{
		super(plugIn);
	}

	public boolean onCommand(CommandSender sender, Command command, String label, String[] field) 
	{	
		if(isAbleToUseCommand(sender))
		{	
			//Constructing the Reason for the tempban action:
			
			//If the entries on the command are greater than or equal to 4, which means they entered the minimum required fields:
			if(field.length >= 4)
			{
				boolean executed = executeTempBan(sender, field);
				return executed;
			}
		}
		return false;
	}

	/**
	 * Time measurement in 'seconds' in milliseconds.
	 */
	static int TIME_SECOND = 1000;
	
	/**
	 * Time measurement in 'minutes' in milliseconds.
	 */
	static int TIME_MINUTE = 60000;

	/**
	 * Time measurement in 'hours' in milliseconds.
	 */
	static int TIME_HOUR = 3600000;
	
	/**
	 * Time measurement in 'days' in milliseconds.
	 */
	static int TIME_DAY = 86400000;
	
	/**
	 * Method to give time measurement in milliseconds.
	 * @param string - input
	 * @return time measurement in milliseconds.
	 */
	private int getTimeMeasurement(String string) 
	{
		//If the time entered is measured in seconds:
		if(string.toLowerCase().equals("s"))
		{
			//Return the time measurement of "seconds".
			return TIME_SECOND;
		}
		//If the time entered is measured in minutes:
		else if(string.toLowerCase().equals("m"))
		{
			//Return the time measurement of "minutes".
			return TIME_MINUTE;
		}
		//If the time entered is measured in hours:
		else if(string.toLowerCase().equals("h"))
		{
			//Return the time measurement of "hours".
			return TIME_HOUR;
		}
		//If the time entered is measured in days:
		else if(string.toLowerCase().equals("d"))
		{
			//Return the time measurement of "days".
			return TIME_DAY;
		}
		else
		{
			//If the time measurement input is not syntaxed correctly, this will return a -1;
			return -1;
		}
	}
	
	
	public boolean executeTempBan(CommandSender sender, String[] field)
	{
		//From here on we will attempt to execute this action, provided the amount of fields entered is sufficient and valid.
		
		//String to build the reason.
		String reason = field[3];
		
		//For every space after the start of the reason in the command, concatinate the strings with a space provided between each.
		for(int x = 4; x < field.length; x++)
		{
			reason += " " + field[x];
		}
		
		long timeBanned;
		
		// Getting time measurement
		int timeMeasurement = getTimeMeasurement(field[2]);
		
		// If the time input was incorrect syntax
		if(timeMeasurement == -1)
		{
			//returns false to return syntax.
			return false;
		}
		// Else the time is correct then do this:
		else
		{
			//integer to get the input for time.
			int inputTime;
			
			//tries to convert the String object reperesentation of the time variable input for the banning time:
			try
			{							
				inputTime = Integer.parseInt(field[1]);
			}
			
			//If this does not work then we simply return false to show the correct syntax of the command.
			catch(java.lang.NumberFormatException e)
			{
				return false;
			}

			//Time
			Date now = new Date();
			
			//Calculates the time in milliseconds for when the ban will be lifted:
			Date unBannedDate = new Date(now.getTime() + (long)inputTime * timeMeasurement);
			
			//sets timeBanned to timeCurrent:
			timeBanned = unBannedDate.getTime();
		}
		
		//Gets the player Object:
		OfflinePlayer playerBanned = null;// = Bukkit.getPlayerExact(field[0]);
		
		//If the player Object is not defined properly:
		if(playerBanned == null)
		{
			//Fetch a OfflinePlayer object:
			playerBanned = Bukkit.getOfflinePlayer(field[0]);
		}
		
		//Performs the tempban action:
		
		//To record the data:
		Gson gson = new Gson();
		RecordTempBan recordTempBan = new RecordTempBan();
		recordTempBan.executedOffline = plugIn.isOnlineMode();
		recordTempBan.issuedBy = sender.getName();
		recordTempBan.playerIssued = playerBanned.getName();
		recordTempBan.Reason = reason;
		recordTempBan.timeBanned = timeBanned;
		RecordFileHandler.writeJSONFile(new File(RecordFileHandler.FOLDER_RECORD_TEMPBAN.getAbsolutePath() + "/" + playerBanned.getName() + ".json"), gson.toJson(recordTempBan));
		
		if(playerBanned.isOnline())
		{
			Date date = new Date(timeBanned);
			playerBanned.getPlayer().kickPlayer("Temporary Ban" + "\n" + ChatColor.GREEN + "\"" + reason + "\"" + "\n\n" + ChatColor.WHITE + "You are TempBanned until: " + "\n" + ChatColor.GREEN + ChatColor.ITALIC + DateFormat.getInstance().format(date));
			
		}
		return true;
	}

}
