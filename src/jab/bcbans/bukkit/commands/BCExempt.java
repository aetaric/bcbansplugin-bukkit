package jab.bcbans.bukkit.commands;

import jab.bcbans.bukkit.BukkitPlugIn;
import jab.bcbans.core.BCPlayer;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class BCExempt extends BCCommand implements CommandExecutor
{

	public BCExempt(BukkitPlugIn plugIn) 
	{
		super(plugIn);
	}

	public boolean onCommand(CommandSender sender, Command command, String label, String[] field) 
	{
		if(isAbleToUseCommand(sender))
		{			
			if(field.length > 0)
			{
				if(field.length == 1)
				{
					if(field[0].toLowerCase().equals("refresh"))
					{
						plugIn.core.refreshExemptions();
						return true;
					}
				}
				else if(field.length >= 2)
				{
					if(field[0].toLowerCase().contains("add"))
					{
						if(field.length == 3)
						{							
							boolean execute = plugIn.core.addExemption(new BCPlayer(field[1],""),field[2]);
							if(execute)
							{
								sender.sendMessage("Exemption added.");
							}
							else
							{
								sender.sendMessage("Exemption already added.");
							}
							return true;
						}
						else if(field.length == 2)
						{
							boolean execute = plugIn.core.addExemption(new BCPlayer(field[1],""),"No Reason Given.");
							if(execute)
							{
								sender.sendMessage("Exemption added.");
							}
							else
							{
								sender.sendMessage("Exemption already added.");
							}
							return true;
						}
					}
					else if(field[0].toLowerCase().contains("remove"))
					{
						boolean execute = plugIn.core.removeExemption(new BCPlayer(field[1],""));
						if(execute)
						{
							sender.sendMessage("Exemption removed.");
						}
						else
						{
							sender.sendMessage("Exemption does not exist.");
						}
						return true;
					}
				}
			}
			else
			{
				return false;
			}
		}
		return false;
	}

}
