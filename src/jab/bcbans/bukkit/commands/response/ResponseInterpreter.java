package jab.bcbans.bukkit.commands.response;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

/**
 * Class to keep the JSON Java Library clean and send to Bukkit through this route.
 * @author JabJabJab
 *
 */
public class ResponseInterpreter 
{
	
	public static void sendPlayerMessage(String playerName, String message)
	{
		Player playerToMessage = Bukkit.getPlayer(playerName);
		if(playerToMessage.isOnline())
		{
			playerToMessage.sendMessage(message);
		}
	}
	
	public static void sendPlayerMultipleMessages(String playerName, String[] messages)
	{
		Player playerToMessage = Bukkit.getPlayer(playerName);
		if(playerToMessage.isOnline())
		{
			playerToMessage.sendMessage(messages);
		}
	}
	
	
	
	
}
