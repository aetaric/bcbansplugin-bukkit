package jab.bcbans.bukkit.commands;

import java.io.File;
import java.util.ArrayList;

import jab.bcbans.bukkit.BukkitPlugIn;
import jab.bcbans.core.file.FileHandler;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class BCListWarns extends BCCommand implements CommandExecutor
{

	public BCListWarns(BukkitPlugIn plugIn) 
	{
		super(plugIn);
	}

	public boolean onCommand(CommandSender sender, Command command, String label, String[] field) 
	{
		if(isAbleToUseCommand(sender))
		{	
			if(field.length >= 1)
			{
				String playerName = field[0];
				if(plugIn.getDataFolder().exists())
				{
					String warnFolderString = this.plugIn.getDataFolder().toString() + "/Warns/";
					//Creates 'File' object of the Warns folder.
					File warnFolder = new File(warnFolderString);
					//If the folder does not exist:
					if(warnFolder.exists())
					{
						File warnDocument = new File(warnFolderString + playerName + ".txt");
						if(warnDocument.exists())
						{
							ArrayList<String> warnList = FileHandler.readFile(warnDocument);
							sender.sendMessage("Player has " + warnList.size() + "warnings:");
							for(int warningCount = 0; warningCount < warnList.size(); warningCount++)
							{
								sender.sendMessage("Warning #" + (warningCount + 1) + ": " + warnList.get(warningCount));
							}
						}
						else
						{
							sender.sendMessage("Player " + playerName + " has no warnings.");
						}
					}
				}
				
				return true;
			}
		}
		return false;
	}

}
