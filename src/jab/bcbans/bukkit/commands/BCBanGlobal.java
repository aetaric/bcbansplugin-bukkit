package jab.bcbans.bukkit.commands;

import jab.bcbans.bukkit.BukkitPlugIn;
import jab.bcbans.core.json.object.Ban;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class BCBanGlobal extends BCCommand implements CommandExecutor
{

	public BCBanGlobal(BukkitPlugIn plugIn) 
	{
		super(plugIn);
	}

	@SuppressWarnings("unused")
	public boolean onCommand(CommandSender sender, Command command, String label, String[] field) 
	{
		if(isAbleToUseCommand(sender))
		{			
			if(field.length >= 2)
			{

				String playerName = field[0];
				String reason = field[1];
				for(int x = 2; x < field.length; x++)
				{
					reason += " " + field[x];
				}
				sender.sendMessage(ChatColor.BOLD + "" + ChatColor.ITALIC + "" + ChatColor.GREEN + "Banning Player...");
				Ban ban = plugIn.core.issueGlobalBan(playerName, getSenderName(sender), reason);
				return true;
			}
		}
		return false;
	}

}
