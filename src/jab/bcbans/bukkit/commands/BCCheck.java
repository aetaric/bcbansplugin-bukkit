package jab.bcbans.bukkit.commands;

import jab.bcbans.bukkit.BukkitPlugIn;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class BCCheck extends BCCommand implements CommandExecutor
{

	public BCCheck(BukkitPlugIn plugIn) 
	{
		super(plugIn);
	}

	public boolean onCommand(CommandSender sender, Command command, String label, String[] field) 
	{
		String player = field[0];
		if(isAbleToUseCommand(sender))
		{			
			if(player != null && !player.isEmpty())
			{
				boolean basic = true;
				if(field.length > 1)
				{				
					if(field[1].equals("full"))
					{
						basic = false;
					}
				}
				plugIn.core.issueCheck(field[0],this.getSenderName(sender),basic);
				return true;
			}
		}
		return false;
	}

}
