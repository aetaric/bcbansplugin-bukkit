package jab.bcbans.bukkit;

import java.io.File;

import jab.bcbans.core.BCCore;
import jab.bcbans.core.util.LogSystem;

import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import com.nijiko.permissions.PermissionHandler;

import ru.tehkode.permissions.PermissionManager;
import ru.tehkode.permissions.bukkit.PermissionsEx;

public class BukkitPlugIn  extends JavaPlugin
{
	public BCCore core;
	public BukkitPlugIn plugIn;
	public PluginManager pluginManager;
	public LoginListener loginListener;
	CommandList commandList;
	
	public boolean isOnlineMode()
	{
		return Bukkit.getOnlineMode();
	}
	public static PermissionHandler Permissions = null;
	public static PermissionManager pex = null;
	
	/**
	 * When plug-in is loaded.
	 */
    public void onEnable() 
    {
    	if(!(Bukkit.getOnlineMode()))
    	{
    		LogSystem.logEvent(LogSystem.LOG_FATAL, "SERVER IS ON OFFLINE MODE. BCBANS WILL NOT ENABLE.");
    		pluginManager.disablePlugin(this);
    	}
    	else
    	{
    		enableListeners();
    		enableBCCore();
    		setupPermissions();
    		LogSystem.logEvent(LogSystem.LOG_INFO, "BCBans plugin successfuly loaded");
        	plugIn = this;
    	}
    }
    
    @SuppressWarnings("static-access")
	public void onDisable()
    {
    	core.disableCore();
    }
    
    private void enableListeners() 
    {
    	commandList = new CommandList(this);
		loginListener = new LoginListener(this);		
	}

	private void enableBCCore() 
    {
		core = new BCCore(new File("Plugins/BCBans/BCBans.cfg"),"BUKKIT");
		core.setInterface(new BukkitAction());
		core.enableCore();		
	}

	public void setupPermissions() 
 	{
     	pluginManager = getServer().getPluginManager();
     	pluginManager.registerEvents(loginListener, this);
 		Plugin perms3 = this.getServer().getPluginManager().getPlugin("Permissions");
 		Plugin permsex = this.getServer().getPluginManager().getPlugin("PermissionsEx");
 		if(permsex != null)
 		{
 			PermissionManager permissionsex = PermissionsEx.getPermissionManager();			
 			pex = permissionsex;
 			LogSystem.logEvent(LogSystem.LOG_INFO, " PermissionsEx detected..");
 			return;
 		}
 		if (perms3 != null)
 		{
 			Permissions = ((com.nijikokun.bukkit.Permissions.Permissions) perms3).getHandler();
 			LogSystem.logEvent(LogSystem.LOG_INFO, " Permissions3 detected..");
 			return;
 		}
 		else
 		{
 			LogSystem.logEvent(LogSystem.LOG_INFO, "No supported permissions system found. Falling back to op support");
 			return;
 		}
 	}
}
