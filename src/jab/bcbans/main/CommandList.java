package jab.bcbans.main;

import jab.bcbans.bukkit.commands.BCBan;
import jab.bcbans.bukkit.commands.BCBanGlobal;
import jab.bcbans.bukkit.commands.BCCheck;
import jab.bcbans.bukkit.commands.BCExempt;
import jab.bcbans.bukkit.commands.BCListWarns;
import jab.bcbans.bukkit.commands.BCQuery;
import jab.bcbans.bukkit.commands.BCTempban;
import jab.bcbans.bukkit.commands.BCUnban;
import jab.bcbans.bukkit.commands.BCVerify;
import jab.bcbans.bukkit.commands.BCWarn;

public class CommandList 
{
	public BCQuery bcQuery;
	public BCBan bcBan;
	public BCUnban bcUnban;
	public BCBanGlobal bcBanGlobal;
	public BCCheck bcCheck;
	public BCVerify bcVerify;
	public BCExempt bcExempt;
	public BCWarn bcWarn;
	public BCListWarns bcListWarns;
	public BCTempban bcTempban;
	private BCBans plugin;
	
	public CommandList(BCBans bcBans)
	{
		this.plugin = bcBans;
		bcQuery = new BCQuery(plugin);
		plugin.getCommand("BCquery").setExecutor(bcQuery);
		bcBan = new BCBan(plugin);
		plugin.getCommand("BCban").setExecutor(bcBan);
		BCUnban unban = new BCUnban(plugin);
		plugin.getCommand("BCunban").setExecutor(unban);
		bcBanGlobal = new BCBanGlobal(plugin);
		plugin.getCommand("BCbanglobal").setExecutor(bcBanGlobal);
		bcCheck = new BCCheck(plugin);
		plugin.getCommand("BCCheck").setExecutor(bcCheck);
		bcVerify = new BCVerify(plugin);
		plugin.getCommand("BCverify").setExecutor(bcVerify);
		bcExempt = new BCExempt(plugin);
		plugin.getCommand("BCExempt").setExecutor(bcExempt);
		bcWarn = new BCWarn(plugin);
		plugin.getCommand("BCwarn").setExecutor(bcWarn);
		bcListWarns = new BCListWarns(plugin);
		plugin.getCommand("BCwarns").setExecutor(bcListWarns);
		bcTempban = new BCTempban(plugin);
		plugin.getCommand("BCtempban").setExecutor(bcTempban);
	}
}
