package jab.bcbans.main;


import java.util.logging.Logger;

public class LogSystem 
{
	BCBans plugin;
	public static final String LOG_INFO = "INFO";	
	public static final String LOG_URGENT = "URGENT";
	public static final String LOG_FATAL = "FATAL";
	public static final String LOG_ERROR = "ERROR";	

	public LogSystem(BCBans bcBans) 
	{
		this.plugin = bcBans;
		log = plugin.getLogger();
	}

	private static Logger log;
	
	public static void logEvent(String event, String message)
	{
		String compiledMessage = "" + event + " : " + message + ".";
		log.info(compiledMessage);
	}
}
