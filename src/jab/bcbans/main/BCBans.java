package jab.bcbans.main;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.charset.Charset;

import jab.bcbans.configuration.Configuration;
import jab.bcbans.configuration.ExemptionList;
import jab.bcbans.file.RecordFileHandler;
import jab.bcbans.json.QueryManager;

import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import ru.tehkode.permissions.PermissionManager;
import ru.tehkode.permissions.bukkit.PermissionsEx;

import com.nijiko.permissions.PermissionHandler;

public class BCBans extends JavaPlugin
{
	LogSystem logSystem;
	private static BCBans bcBans;
	CommandList commandList;
	public static Configuration configuration;
	public static PluginManager pluginManager;
	public ExemptionList exemptionList;
	public static boolean isOfflineMode = false;
	public static PermissionHandler Permissions = null;
	public static PermissionManager pex = null;
	public static QueryManager queryManager;
	public LoginListener loginListener;
	
	/**
	 * When plug-in is loaded.
	 */
    public void onEnable() 
    {
    	RecordFileHandler.checkDirectories();
    	if(!Bukkit.getOnlineMode())
    	{
    		LogSystem.logEvent(LogSystem.LOG_FATAL, "SERVER IS ON OFFLINE MODE. BCBANS WILL NOT ENABLE.");
    		pluginManager.disablePlugin(this);
    	}
    	//Initializes our lovely LogSystem class.
    	logSystem = new LogSystem(this);
    	//Sets our 'PluginManager' object accordingly.
    	pluginManager = getServer().getPluginManager();
    	//Initializes our lovely Configurations class.
    	configuration = new Configuration(this);
    	//Initializes our lovely Exemptions list.
    	exemptionList = new ExemptionList(this);
    	loginListener = new LoginListener(this);
    	pluginManager.registerEvents(loginListener, this);
    	commandList = new CommandList(this);
    	queryManager = new QueryManager(this);
		queryManager.shouldDisableThreads = false;
    	setupPermissions();
    	//Tell the API who we are and what 'version' we are running
    	registerWithAPI();
    	LogSystem.logEvent(LogSystem.LOG_INFO, "BCBans plugin successfuly loaded");
    	bcBans = this;
    }
	
	private void registerWithAPI() 
	{
		URL url;
		try
		{
			url = new URL("https://www.bcbans.com/api/register?apikey=" + BCBans.configuration.getAPIKey() 
				+ "&version=BUKKIT");
			InputStream is = url.openStream();
			BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
			String json = rd.readLine();
			if (json.contains("invalid apikey"))
			{
				LogSystem.logEvent(LogSystem.LOG_ERROR, "Your apikey is invalid. " +
						"Please check your config file! BCBans is now disabled!");
				this.getPluginLoader().disablePlugin(this);
			}
		}
		catch (Exception e)
		{
			
		}
	}

	/**
	 * Disable method for when plugin is disabled.
	 */
	public void onDisable() 
    {
        LogSystem.logEvent(LogSystem.LOG_INFO, "BCBans plugin disabled");
    }
	
	public static void disablePlugin()
	{
		queryManager.shouldDisableThreads = true;
		LogSystem.logEvent(LogSystem.LOG_INFO, "BCBans is currently being disabled");
		pluginManager.disablePlugin(bcBans);
	}
	
    public void setupPermissions() 
	{
    	
		Plugin perms3 = this.getServer().getPluginManager().getPlugin("Permissions");
		Plugin permsex = this.getServer().getPluginManager().getPlugin("PermissionsEx");
		//PluginDescriptionFile pdfFile = this.getDescription();
		if(permsex != null)
		{
			PermissionManager permissionsex = PermissionsEx.getPermissionManager();			
			pex = permissionsex;
			LogSystem.logEvent(LogSystem.LOG_INFO, " PermissionsEx detected..");
			return;
		}
		if (perms3 != null)
		{
			Permissions = ((com.nijikokun.bukkit.Permissions.Permissions) perms3).getHandler();
			LogSystem.logEvent(LogSystem.LOG_INFO, " Permissions3 detected..");
			return;
		}
		else
		{
			LogSystem.logEvent(LogSystem.LOG_INFO, "No supported permissions system found. Falling back to op support");
			return;
		}
	}
}
