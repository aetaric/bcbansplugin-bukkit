package jab.bcbans.configuration;

import java.io.File;
import java.util.ArrayList;

import jab.bcbans.file.FileHandler;
import jab.bcbans.main.BCBans;
import jab.bcbans.main.LogSystem;

/**
 * Class designed to handle the configurations made for the plug-in "BCBans".
 * @author JabJabJab
 *
 */
public class Configuration 
{
	/**
	 * Object for accessing the BCBans plug-in.
	 */
	BCBans plugin;

	/**
	 * File Object for Configurations.
	 */
	File fileConfiguration = new File("plugins/BCBans/BCBans.cfg");
	
	/**
	 * Integer for addressing how many log-in threads are running.
	 */
	private int loginThreadCount;
	
	/**
	 * Integer for addressing How many Global Bans someone can have before being rejected by this server.
	 */
	private int banCountLimit;
	
	/**
	 * String to store the API KEY needed to run the plug-in properly.
	 */
	private String KEY_API;
	
	/**
	 * String to store the default ban reason if one has not been defined while processing a ban request.
	 */
	private String banReasonDefault;
	
	/**
	 * String to store the default ban message if one has not been defined while processing a ban request.
	 */
	private String banMessageDefault;
	
	/**
	 * Boolean object for use when debugging the plug-in.
	 */
	public boolean isDebugMode = false;
	
	/**
	 * Object to handle the reading and writing of files.
	 */
	FileHandler fileHandler;
	
	
	/**
	 * Constructor method for the class. Requires 'BCBans' object to process properly.
	 * @param bcbans
	 */
	public Configuration(BCBans bcbans)
	{
		//Setting the 'plug-in' object to BCBans object.
		plugin = bcbans;
		//creates the plug-in directory folder if it has not been made already.
		plugin.getDataFolder().mkdirs();
		//Processes the Configurations data.
		processConfiguration();
	}

	/**
	 * Method to process and load the configurations of 'BCBans' properly.
	 */
	private void processConfiguration() 
	{
		//If the file does not exist:
		if(!fileConfiguration.exists())
		{
			//Writes Configuration file.
			FileHandler.writeFile(fileConfiguration,factoryConfiguration);
			
			//Writes out message to console to prep the configuration file: ----------------------------------------------------\
			LogSystem.logEvent(LogSystem.LOG_FATAL	, "Configurations file not found"										);//|
			LogSystem.logEvent(LogSystem.LOG_INFO	, "Configurations file created in '/plugins/BCBans/' folder"			);//|
			LogSystem.logEvent(LogSystem.LOG_INFO	, "Please configure your plugin configuration file to run this plugin"	);//|
			//------------------------------------------------------------------------------------------------------------------/
			
			//Disables the plug-in entirely.
			BCBans.disablePlugin();
		}
		else
		{
			//Read the configuration file.
			readConfigurationFile();
		}
	}
	
	/**
	 * Reads configuration file and assigns data accordingly.
	 */
	private void readConfigurationFile() 
	{
		ArrayList<String> listStrings = FileHandler.readFile(fileConfiguration);
	    
		//going through each text line inside the config file.
	    for(String string : listStrings)
	    {
	    	//API_KEY line:
	    	if(string.contains("apikey"))
	    	{
	    		//Parses via colon character.
	    		String StringArray[] = string.split(":");
	    		
	    		//assigning the string accordingly.
	    		KEY_API = StringArray[1];
	    		
	    		//continues to optimize time.
	    		continue;
	    	}
	    	else if(string.contains("bancountlimit"))
	    	{
	    		//Parses via colon character.
	    		String StringArray[] = string.split(":");
	    		
	    		//assigning the string accordingly.
	    		try
	    		{		   
	    			//Parses the string representing the ban count number in the configuration file to a Integer.
	    			banCountLimit = Integer.parseInt(StringArray[1]);
	    		}
	    		
	    		//If number entry is not valid.
	    		catch(NumberFormatException e)
	    		{
	    			//Logs issue.
	    			LogSystem.logEvent(LogSystem.LOG_ERROR , "BAN COUNT LIMIT IS NOT A VALID NUMBER. Defaulting to 2.");
	    			
	    			//sets banCountLimit to default of '2';
	    			banCountLimit = 2;
		    		
	    			//continues to optimize time.
	    			continue;
	    		}
	    		
	    		//continues to optimize time.
	    		continue;
	    	}
	    	else if(string.contains("default-ban-reason"))
	    	{
	    		//Parses via colon character.
	    		String StringArray[] = string.split(":");
	    		
	    		//assigning the string accordingly.
	    		banReasonDefault = StringArray[1];
	    		
	    		//continues to optimize time.
	    		continue;
	    	}
	    	else if(string.contains("default-ban-message"))
	    	{
	    		//Parses via colon character.
	    		String StringArray[] = string.split(":");
	    		
	    		//assigning the string accordingly.
	    		banMessageDefault = StringArray[1];
	    		
	    		//continues to optimize time.
	    		continue;
	    	}
	    	else if(string.contains("loginthreadcount"))
	    	{
	    		//Parses via colon character.
	    		String StringArray[] = string.split(":");
	    		
	    		//assigning the string accordingly.
	    		try 
	    		{		    			
	    			loginThreadCount  = Integer.parseInt(StringArray[1]);
	    		}
	    		//Catches exception for processing number conversion failure:
	    		catch(NumberFormatException e)
	    		{
	    			LogSystem.logEvent(LogSystem.LOG_ERROR, "Invald Thread Count entry (Bcbans.cfg), using 1 thread");
	    			loginThreadCount = 1;
	    			continue;
	    		}
	    		//continues to optimize time.
	    		continue;
	    	}
	    	//If DebugMode is defined in the txt file.
	    	else if(string.contains("debugmode"))
	    	{
	    		//Splits string for boolean.
	    		String StringArray[] = string.split(":");
	    		
	    		//If the result is true:
	    		if(StringArray[1].toLowerCase().contains("true"))
	    		{
	    			//sets debugmode to true.
	    			this.isDebugMode = true;
	    		}
	    		//continues to save operation time.
	    		continue;
	    	}
	    }
	}

	/**
	 * Returns loginThreadCount.
	 * @return
	 */
	public int getLoginThreadCount()
	{
		return this.loginThreadCount;
	}
	
	/**
	 * Returns GlobalBanLimit.
	 * @return
	 */
	public int getGlobalBanLimit()
	{
		return banCountLimit;
	}
	
	/**
	 * Returns the APIKey used.
	 * @return
	 */
	public String getAPIKey()
	{
		return KEY_API;
	}
	
	/**
	 * Returns the String for the Default Ban Reason.
	 * @return
	 */
	public String getDefaultBanReason()
	{
		return banReasonDefault;
	}
	
	/**
	 * Returns DebugMode boolean.
	 * @return
	 */
	public boolean isDebugMode()
	{
		return false;
	}
	
	/**
	 * Returns the Default Ban Message.
	 * @return
	 */
	public String getDefaultBanMessage()
	{
		return banMessageDefault;
	}
	
	/**
	 * System property for a new line inside of a text document.
	 */
	String newline = System.getProperty("line.separator");
	
	/**
	 * String Array for the default configurations for new configuration.
	 */
	String[] factoryConfiguration = new String[]
		{
			"#Get an API key from bcbans.com" + newline,
	        "apikey:insert key here" + newline,
	        "#Amount of bans that are considered the maximum amount to play the service." + newline,
	        "bancountlimit:2" + newline,
	        "#Reason to cast to the ban if the ban has no reason defined." + newline,
	        "default-ban-reason:No reason specified" + newline,
	        "#Ban message for people who attempt to get on and get rejected." + newline,
	        "default-ban-message:You have been banned from this server" + newline,   
	        "#Amount of threads running to check players logging in. (MIN:1 Recommended Max:3)" + newline,
	        "loginthreadcount:1"
		};
}
