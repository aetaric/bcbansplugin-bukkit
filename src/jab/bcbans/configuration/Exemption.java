package jab.bcbans.configuration;

/**
 * Class for handling Exemption objects.
 * @author JabJabJab
 *
 */
public class Exemption 
{
	/**
	 * Name of the exemption player.
	 */
	private String exemptionName;
	
	/**
	 * Date of the exemption player.
	 */
	private String exemptionDate;
	
	/**
	 * Reason for the exemption.
	 */
	private String exemptionReason;
	
	/**
	 * Sets the name of the 'Exemption' Object.
	 * @param name
	 */
	public void setExemptionName(String name)
	{
		exemptionName = name;
	}
	
	/**
	 * Sets the reason of the 'Exemption' Object.
	 * @param reason
	 */
	public void setExemptionReason(String reason)
	{
		exemptionReason = reason;
	}
	
	/**
	 * Sets the date of the 'Exemption' Object.
	 * @param name
	 */
	public void setExemptionDate(String date)
	{
		exemptionDate = date;	
	}
	
	/**
	 * Returns the name of the 'Exemption' Object.
	 * @param name
	 */
	public String getExemptionName()
	{
		return exemptionName;
	}
	
	/**
	 * Returns the date of the 'Exemption' Object.
	 * @param name
	 */
	public String getExemptionDate()
	{
		return exemptionDate;
	}
	
	/**
	 * Returns the reason of the 'Exemption' Object.
	 * @param name
	 */
	public String getExemptionReason()
	{
		return exemptionReason;
	}
}
